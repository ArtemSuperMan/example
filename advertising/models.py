from django.db import models
from  blog.models import ParentCategory


class AdvertisingBlog(models.Model):      

    choices_position = (                                                                       

           ('Left','Слева',),  
           ('bottom','В низу',)          
    )                                  

           
    сustomer = models.CharField(max_length=400,verbose_name='Заказчик')                       
 
    parent_category = models.ForeignKey(ParentCategory,verbose_name='Тема портала')                          

    position = models.CharField(max_length=400,verbose_name='Позиция',choices=choices_position)

    url = models.CharField(max_length=400,verbose_name='Ссылка')

    image = models.ImageField(upload_to='images/', blank=True, verbose_name='Фотография')

    date_time_to = models.DateTimeField(verbose_name='Срок дейтвия')              

    is_active = models.BooleanField(verbose_name='Активен',default=True)      


    class Meta:      
        verbose_name = "Рекламу в инфо-портале"          
        verbose_name_plural = "Реклама в инфо-портале"        

    def __str__(self):        
        return  self.сustomer




class AdvertisingFilter(models.Model):      

    choices_position = (                                                                       

           ('left','Слева',),  
           ('right','Справа',)          
    )                                  

           
    сustomer = models.CharField(max_length=400,verbose_name='Заказчик')                                                        

    position = models.CharField(max_length=40,verbose_name='Позиция',choices=choices_position,unique=True)

    url = models.CharField(max_length=400,verbose_name='Ссылка')

    image = models.ImageField(upload_to='images/', blank=True, verbose_name='Фотография')

    date_time_to = models.DateTimeField(verbose_name='Срок дейтвия')              

    is_active = models.BooleanField(verbose_name='Активен',default=True)        


    class Meta:      
        verbose_name = "Рекламу в фильтре"        
        verbose_name_plural = "Реклама в фильтре"        

    def __str__(self):        
        return  self.сustomer                    
