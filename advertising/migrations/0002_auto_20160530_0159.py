# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advertising', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advertisingblog',
            name='url',
            field=models.CharField(verbose_name='Ссылка', max_length=400),
        ),
    ]
