# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advertising', '0002_auto_20160530_0159'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdvertisingFilter',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('сustomer', models.CharField(verbose_name='Заказчик', max_length=400)),
                ('position', models.CharField(verbose_name='Позиция', choices=[('left', 'Слева'), ('right', 'Справа')], unique=True, max_length=40)),
                ('url', models.CharField(verbose_name='Ссылка', max_length=400)),
                ('image', models.ImageField(verbose_name='Фотография', blank=True, upload_to='images/')),
                ('date_time_to', models.DateTimeField(verbose_name='Срок дейтвия')),
                ('is_active', models.BooleanField(verbose_name='Активен', default=True)),
            ],
            options={
                'verbose_name_plural': 'Реклама в фильтре',
                'verbose_name': 'Рекламу в фильтре',
            },
        ),
        migrations.AlterModelOptions(
            name='advertisingblog',
            options={'verbose_name_plural': 'Реклама в инфо-портале', 'verbose_name': 'Рекламу в инфо-портале'},
        ),
    ]
