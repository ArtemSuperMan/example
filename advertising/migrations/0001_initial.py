# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20160523_2031'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdvertisingBlog',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('сustomer', models.CharField(verbose_name='Заказчик', max_length=400)),
                ('position', models.CharField(choices=[('Left', 'Слева'), ('bottom', 'В низу')], verbose_name='Позиция', max_length=400)),
                ('url', models.CharField(choices=[('Left', 'Слева'), ('bottom', 'В низу')], verbose_name='Ссылка', max_length=400)),
                ('image', models.ImageField(blank=True, upload_to='images/', verbose_name='Фотография')),
                ('date_time_to', models.DateTimeField(verbose_name='Срок дейтвия')),
                ('is_active', models.BooleanField(default=True, verbose_name='Активен')),
                ('parent_category', models.ForeignKey(to='blog.ParentCategory', verbose_name='Тема портала')),
            ],
            options={
                'verbose_name_plural': 'Рекламу ',
                'verbose_name': 'Реклама ',
            },
        ),
    ]
