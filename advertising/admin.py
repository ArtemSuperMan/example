from django.contrib import admin
from .models import *

#   
@admin.register(AdvertisingBlog)                        
class AdminAdvertisingBlog(admin.ModelAdmin):
    
    list_filter = ('сustomer','parent_category__title','position',) 

    list_display = ('сustomer','parent_category','position','date_time_to','is_active')         


@admin.register(AdvertisingFilter)                        
class AdminAdvertisingFilter(admin.ModelAdmin):
    
    list_filter = ('position',)

    list_display = ('сustomer','position','date_time_to','is_active')            
