from django.db import models
from imagekit.models import ProcessedImageField  
from imagekit.processors import ResizeToFill


# watermark    

# Фотографии на главной                                                                                                                  
class homeImage(models.Model):                                                                    

               
    header_image = ProcessedImageField(upload_to='homeImages/',verbose_name='Фотография в верху',  
                                           processors=[ResizeToFill(1300,867)],  
                                           format='JPEG',  
                                           options={'quality':100})

    Rent_image = ProcessedImageField(upload_to='homeImages/',verbose_name='Фотография аренда',
                                           processors=[ResizeToFill(500,300)],  
                                           format='JPEG',  
                                           options={'quality':100})  

    Rent_Sale = ProcessedImageField(upload_to='homeImages/',verbose_name='Фотография продажа',
                                           processors=[ResizeToFill(500,300)],  
                                           format='JPEG',  
                                           options={'quality':100})

    Rent_info = ProcessedImageField(upload_to='homeImages/',verbose_name='Фотография инфо-портал',
                                           processors=[ResizeToFill(500,300)],  
                                           format='JPEG',  
                                           options={'quality':100})    

    Rent_ipoteka = ProcessedImageField(upload_to='homeImages/',verbose_name='Фотография ипотека',
                                           processors=[ResizeToFill(500,300)],    
                                           format='JPEG',  
                                           options={'quality':100})


    class Meta:      
        verbose_name = "Фотографии на главной"            
        verbose_name_plural = "Фотографии на главной"    

           
 
