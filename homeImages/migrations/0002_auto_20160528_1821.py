# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homeImages', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='homeimage',
            options={'verbose_name_plural': 'Фотографии на главной', 'verbose_name': 'Фотографии на главной'},
        ),
    ]
