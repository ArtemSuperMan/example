# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='homeImage',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('header_image', imagekit.models.fields.ProcessedImageField(upload_to='homeImages/', verbose_name='Фотография в верху')),
                ('Rent_image', imagekit.models.fields.ProcessedImageField(upload_to='homeImages/', verbose_name='Фотография аренда')),
                ('Rent_Sale', imagekit.models.fields.ProcessedImageField(upload_to='homeImages/', verbose_name='Фотография продажа')),
                ('Rent_info', imagekit.models.fields.ProcessedImageField(upload_to='homeImages/', verbose_name='Фотография инфо-портал')),
                ('Rent_ipoteka', imagekit.models.fields.ProcessedImageField(upload_to='homeImages/', verbose_name='Фотография ипотека')),
            ],
        ),
    ]
