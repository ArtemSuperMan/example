from django.conf.urls import url
from . import views


#
urlpatterns = [                    

    url(r'^$', views.get_tariffs),
    url(r'^list/$', views.get_tariffs),  
    url(r'^set/$', views.set_tariffs),
    url(r'^descriptions/$', views.descriptions_tariffs),
    url(r'^paymentmethods/$', views.payment_methods_tariffs),
    url(r'^shareslist/$', views.shares_list),   
    url(r'^result/$', views.oprder_tariffs),
    url(r'^success/$', views.tariffs_success),
    url(r'^fail/$', views.tariffs_fail),
    url(r'^getmylog/$', views.tariffs_get_my_log),
    url(r'^mytariffs/$', views.get_my_tariffs)  
                            
               
]     
