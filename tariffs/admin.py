from django.contrib import admin
from tariffs.models import *    


@admin.register(TariffsOrdersLogs)                      
class AdminObjectLogs(admin.ModelAdmin):        
    

    list_display = ('inv_id','user','tariff_type','out_summ','date_time',)  


@admin.register(Tariffs)                    
class AdminObject(admin.ModelAdmin):   
    

    list_display = ('tariffs_type','validity_date','price','shares')  



@admin.register(TariffsOrders)          
class AdminObjectOrders(admin.ModelAdmin):      
    list_display = ('tariff_type','date_time_to','is_active','user_id',)

    list_filter = ('user',)    


@admin.register(Shares)              
class AdminObjectShares(admin.ModelAdmin):

    list_display = ('title','descriptions','discount','date_time_to','is_active',)

    class Media:        
        js = [ 
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js', '/static/grappelli/tinymce_setup/tinymce_setup.js',
            '/static/grappelli/tinymce_setup/content_typography.css'  
        ] 

@admin.register(DescriptionsTariffs)                
class AdminObjectDescriptions(admin.ModelAdmin):

    list_display = ('descriptions',)


    class Media:        
        js = [ 
        	'/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js', '/static/grappelli/tinymce_setup/tinymce_setup.js',
        	'/static/grappelli/tinymce_setup/content_typography.css'  
        ]


@admin.register(PaymentMethodsTariffs)                
class AdminObjectPaymentMethodsTariffs(admin.ModelAdmin):

    list_display = ('descriptions',)           


    class Media:        
        js = [ 
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js', '/static/grappelli/tinymce_setup/tinymce_setup.js',
            '/static/grappelli/tinymce_setup/content_typography.css'  
        ]                  

