# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0004_auto_20160405_1017'),
    ]

    operations = [
        migrations.CreateModel(
            name='TariffsOrders',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('user_id', models.IntegerField(verbose_name='Айди юзера')),
                ('tariff_type', models.CharField(verbose_name='Названия тарифа', max_length=400)),
                ('date_time_to', models.DateTimeField(verbose_name='Срок дейтвия')),
            ],
        ),
        migrations.AlterField(
            model_name='tariffs',
            name='validity_date',
            field=models.CharField(verbose_name='срок дейтвия', max_length=100, choices=[('days=1', '1 день'), ('days=7', '1 неделя'), ('days=30', '1 месяц'), ('days=180', 'Пол года'), ('days=365', 'Год')]),
        ),
    ]
