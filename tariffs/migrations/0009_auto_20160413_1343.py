# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0008_auto_20160411_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shares',
            name='discount',
            field=models.IntegerField(verbose_name='Процент  скидки'),
        ),
        migrations.AlterField(
            model_name='tariffs',
            name='descriptions',
            field=models.TextField(null=True, verbose_name='описания типов недвижимости'),
        ),
        migrations.AlterField(
            model_name='tariffsorders',
            name='user',
            field=models.ForeignKey(null=True, blank=True, to='users.Users', verbose_name='Пользователь'),
        ),
    ]
