# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20160529_1806'),
        ('tariffs', '0012_remove_tariffs_descriptions'),
    ]

    operations = [
        migrations.CreateModel(
            name='TariffsOrdersLogs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('tariff_type', models.CharField(verbose_name='Названия тарифа', max_length=400, choices=[('Rental_of_property', 'Аренда жилой'), ('Sale_of_residential', 'Продажа жилой'), ('Rent_commercial', 'Аренда коммерческой'), ('Sales_of_commercial', 'Продажа коммерческой'), ('Sale_and_lease_of_land', 'Продажа и аренда земельного участка')])),
                ('out_summ', models.CharField(verbose_name='Сумма заказа', max_length=100, null=True)),
                ('inv_id', models.CharField(verbose_name='Ид заказа', max_length=100, null=True)),
                ('date_time', models.DateTimeField(verbose_name='Дата покупки', auto_now=True)),
                ('user', models.ForeignKey(to='users.Users', null=True, blank=True, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name_plural': 'лог покупок',
                'verbose_name': 'лог покупок',
            },
        ),
    ]
