# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0014_paymentmethodstariffs'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='paymentmethodstariffs',
            options={'verbose_name_plural': 'способы оплаты', 'verbose_name': 'способы оплаты'},
        ),
    ]
