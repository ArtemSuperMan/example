# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0003_auto_20160405_1016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tariffs',
            name='validity_date',
            field=models.CharField(verbose_name='срок дейтвия', max_length=100, choices=[('1 day', '1 день'), ('1 Week', '1 неделя'), ('1 month', '1 месяц'), ('6 months', 'Пол года'), ('year', 'Год')]),
        ),
    ]
