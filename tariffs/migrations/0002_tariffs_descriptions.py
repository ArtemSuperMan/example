# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tariffs',
            name='descriptions',
            field=models.TextField(verbose_name='описания тарифа', null=True),
        ),
    ]
