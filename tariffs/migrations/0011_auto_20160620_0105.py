# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0010_auto_20160523_2031'),
    ]

    operations = [
        migrations.CreateModel(
            name='DescriptionsTariffs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('descriptions', models.TextField(verbose_name='описания тарифов', null=True)),
            ],
            options={
                'verbose_name': 'описания тарифов',
                'verbose_name_plural': 'описания тарифов',
            },
        ),
        migrations.AlterModelOptions(
            name='shares',
            options={'verbose_name': 'Акции', 'verbose_name_plural': 'Акции'},
        ),
        migrations.AlterModelOptions(
            name='tariffs',
            options={'verbose_name': 'тарифы', 'verbose_name_plural': 'тарифы'},
        ),
        migrations.AlterModelOptions(
            name='tariffsorders',
            options={'verbose_name': 'тарифы  товары', 'verbose_name_plural': 'тарифы  товары'},
        ),
    ]
