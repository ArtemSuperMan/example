# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('tariffs', '0007_auto_20160411_1259'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tariffsorders',
            name='user_id',
        ),
        migrations.AddField(
            model_name='tariffsorders',
            name='user',
            field=models.ForeignKey(verbose_name='акция', to='users.Users', blank=True, null=True),
        ),
    ]
