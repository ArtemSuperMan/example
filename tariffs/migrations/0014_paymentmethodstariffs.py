# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0013_tariffsorderslogs'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentMethodsTariffs',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('descriptions', models.TextField(null=True, verbose_name='способы оплаты')),
            ],
            options={
                'verbose_name_plural': 'описания тарифов',
                'verbose_name': 'описания тарифов',
            },
        ),
    ]
