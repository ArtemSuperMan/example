# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0006_tariffsorders_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Shares',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=400, verbose_name='Названия акции')),
                ('descriptions', models.TextField(null=True, verbose_name='описания тарифа')),
                ('discount', models.IntegerField(verbose_name='Скидка в рублях')),
                ('date_time_to', models.DateField(verbose_name='Срок дейтвия акции')),
                ('is_active', models.BooleanField(verbose_name='Активена', default=True)),
            ],
        ),
        migrations.AddField(
            model_name='tariffs',
            name='shares',
            field=models.ForeignKey(verbose_name='акция', null=True, to='tariffs.Shares', blank=True),
        ),
    ]
