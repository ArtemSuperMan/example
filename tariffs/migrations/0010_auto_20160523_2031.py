# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0009_auto_20160413_1343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tariffsorders',
            name='tariff_type',
            field=models.CharField(choices=[('Rental_of_property', 'Аренда жилой'), ('Sale_of_residential', 'Продажа жилой'), ('Rent_commercial', 'Аренда коммерческой'), ('Sales_of_commercial', 'Продажа коммерческой'), ('Sale_and_lease_of_land', 'Продажа и аренда земельного участка')], verbose_name='Названия тарифа', max_length=400),
        ),
    ]
