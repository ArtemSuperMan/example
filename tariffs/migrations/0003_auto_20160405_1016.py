# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tariffs', '0002_tariffs_descriptions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tariffs',
            name='validity_date',
            field=models.CharField(unique=True, verbose_name='срок дейтвия', choices=[('1 day', '1 день'), ('1 Week', '1 неделя'), ('1 month', '1 месяц'), ('6 months', 'Пол года'), ('year', 'Год')], max_length=100),
        ),
    ]
