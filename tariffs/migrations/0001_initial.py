# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tariffs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tariffs_type', models.CharField(verbose_name='Названия тарифа', max_length=400, choices=[('Rental_of_property', 'Аренда жилой'), ('Sale_of_residential', 'Продажа жилой'), ('Rent_commercial', 'Аренда коммерческой'), ('Sales_of_commercial', 'Продажа коммерческой'), ('Sale_and_lease_of_land', 'Продажа и аренда земельного участка')])),
                ('validity_date', models.CharField(verbose_name='срок дейтвия', max_length=100, choices=[('1 day', '1 день'), ('1 Week', '1 неделя'), ('1 month', '1 месяц'), ('6 months', 'Пол года'), ('year', 'Год')])),
                ('price', models.IntegerField(verbose_name='Стоимость в рублях')),
            ],
        ),
    ]
