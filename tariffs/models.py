from django.db import models
from users.models import *  


#  
class Shares(models.Model):                 

   # Названия акции   
    title = models.CharField(max_length=400,verbose_name='Названия акции')

    # описания акции                    
    descriptions = models.TextField(verbose_name='описания тарифа',null=True) 
    
    # Скидка в рублях акции     
    discount =  models.IntegerField(verbose_name='Процент  скидки')
    
    # срок дейтвия акции                                
    date_time_to = models.DateField(verbose_name='Срок дейтвия акции')

    #  Активена       
    is_active = models.BooleanField(verbose_name='Активена',default=True)  


    class Meta:      
        verbose_name = "Акции"        
        verbose_name_plural = "Акции" 


    def __str__(self):                              
        return  self.title               





# тарифы  товары                                                                                
class Tariffs(models.Model):      


    shares = models.ForeignKey(Shares,verbose_name='акция',null=True,blank=True)            

    
    choices_values = {               

       'Rental_of_property':'Аренда жилой',
       'Sale_of_residential':'Продажа жилой',
       'Rent_commercial':'Аренда коммерческой',
       'Sales_of_commercial':'Продажа коммерческой',  
       'Sale_and_lease_of_land':'Продажа и аренда земельного участка',
       'days=1':'1 день',
       'days=7':'1 неделя',
       'days=30':'1 месяц',
       'days=180':'Пол года',  
       'days=365':'Год', 

    }                                                                                                 
    
    # тарифы             
    TARRIFFS_TYPE = (                                                                       

           ('Rental_of_property','Аренда жилой',),
           ('Sale_of_residential','Продажа жилой',),
           ('Rent_commercial','Аренда коммерческой',),
           ('Sales_of_commercial','Продажа коммерческой',),  
           ('Sale_and_lease_of_land','Продажа и аренда земельного участка',), 
                        

    )      
    tariffs_type = models.CharField(max_length=400,verbose_name='Названия тарифа',choices=TARRIFFS_TYPE)
    # тарифы               

    # срок дейтвия                    
    VALIDITY_DATE_TYPE = (                                                                        

           ('days=1','1 день',),
           ('days=7','1 неделя',),
           ('days=30','1 месяц',),
           ('days=180','Пол года',),  
           ('days=365','Год',), 
    )    

    validity_date = models.CharField(max_length=100,verbose_name='срок дейтвия',choices=VALIDITY_DATE_TYPE)  
    # срок дейтвия     

    # Стоимость в рублях
    price = models.IntegerField(verbose_name='Стоимость в рублях')


    def get_name_tariff_type(self):    
        return self.choices_values[self.tariffs_type] 

    def get_name_validity_date(self):  
        return self.choices_values[self.validity_date] 

    class Meta:      
        verbose_name = "тарифы"        
        verbose_name_plural = "тарифы" 



# тарифы  товары                                                                                           
class TariffsOrders(models.Model):   

    choices_values = {               

       'Rental_of_property':'Аренда жилой',
       'Sale_of_residential':'Продажа жилой',
       'Rent_commercial':'Аренда коммерческой',
       'Sales_of_commercial':'Продажа коммерческой',  
       'Sale_and_lease_of_land':'Продажа и аренда земельного участка',
       'days=1':'1 день',
       'days=7':'1 неделя',
       'days=30':'1 месяц',
       'days=180':'Пол года',  
       'days=365':'Год', 

    }    

           
    user = models.ForeignKey(Users,verbose_name='Пользователь',null=True,blank=True)     

    tariff_type = models.CharField(max_length=400,verbose_name='Названия тарифа',choices=Tariffs.TARRIFFS_TYPE)

    date_time_to = models.DateTimeField(verbose_name='Срок дейтвия')

    is_active = models.BooleanField(verbose_name='Активен',default=True)


    def get_name_tariff_type(self):     
        return self.choices_values[self.tariff_type]  


    class Meta:      
        verbose_name = "тарифы  товары"        
        verbose_name_plural = "тарифы  товары" 



# лог покупок                                                                                                      
class TariffsOrdersLogs(models.Model):        

    choices_values = {               

       'Rental_of_property':'Аренда жилой',
       'Sale_of_residential':'Продажа жилой',
       'Rent_commercial':'Аренда коммерческой',
       'Sales_of_commercial':'Продажа коммерческой',  
       'Sale_and_lease_of_land':'Продажа и аренда земельного участка',
       'days=1':'1 день',
       'days=7':'1 неделя',
       'days=30':'1 месяц',
       'days=180':'Пол года',  
       'days=365':'Год', 

    }           

           
    user = models.ForeignKey(Users,verbose_name='Пользователь',null=True,blank=True)           

    tariff_type = models.CharField(max_length=400,verbose_name='Названия тарифа',choices=Tariffs.TARRIFFS_TYPE)

    out_summ = models.CharField(verbose_name='Сумма заказа',max_length=100,null=True)

    inv_id = models.CharField(verbose_name='Ид заказа',max_length=100,null=True)  

    date_time = models.DateTimeField(verbose_name='Дата покупки',auto_now=True)   


    def get_name_tariff_type(self):    
        return self.choices_values[self.tariff_type]    


    class Meta:      
        verbose_name = "лог покупок"        
        verbose_name_plural = "лог покупок"  




#      
class DescriptionsTariffs(models.Model):                         


    # описания акции                    
    descriptions = models.TextField(verbose_name='описания тарифов',null=True)


    class Meta:      
        verbose_name = "описания тарифов"        
        verbose_name_plural = "описания тарифов"   




class PaymentMethodsTariffs(models.Model):                               

    # описания акции                    
    descriptions = models.TextField(verbose_name='способы оплаты',null=True)


    class Meta:      
        verbose_name = "способы оплаты"        
        verbose_name_plural = "способы оплаты"   

 
