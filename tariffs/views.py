from django.db.models import Q
from tariffs.models import *
from django.http import HttpResponse
from django.template import RequestContext  
from django.shortcuts import render_to_response
from django.core import serializers
import json
from datetime import datetime,timedelta 
from django.utils import timezone 
from django.core.exceptions import ObjectDoesNotExist 
from decimal import * 
from hashlib import md5
import random             
# from django.template.loader import render_to_string                 


#                                                                    
def home(request):                  
               
    
    return render_to_response('tariffs_pages.html',{},RequestContext(request))


#                                                                  
def get_tariffs(request):                          

    Rental_of_property = Tariffs.objects.filter(tariffs_type='Rental_of_property') 

    Sale_of_residential = Tariffs.objects.filter(tariffs_type='Sale_of_residential')

    Rent_commercial = Tariffs.objects.filter(tariffs_type='Rent_commercial')

    Sales_of_commercial = Tariffs.objects.filter(tariffs_type='Sales_of_commercial')

    Sale_and_lease_of_land = Tariffs.objects.filter(tariffs_type='Sale_and_lease_of_land')

    template_contex = {          

      'Rental_of_property':Rental_of_property,
      'Sale_of_residential':Sale_of_residential,
      'Rent_commercial':Rent_commercial,
      'Sales_of_commercial':Sales_of_commercial,
      'Sale_and_lease_of_land':Sale_and_lease_of_land  

    }                            
    
    return render_to_response('tariffs_list.html',template_contex,RequestContext(request)) 



#                                                                               
def set_tariffs(request):            


    Shp_user_id = request.session.get('USER',False)

    if Shp_user_id:   


        id_tarrifs = {                       

           'Rental_of_property':request.GET.get('rental_of_property',-1),
           'Sale_of_residential':request.GET.get('sale_of_residential',-1),
           'Rent_commercial' : request.GET.get('rent_commercial',-1),
           'Sales_of_commercial':request.GET.get('sales_of_commercial',-1),
           'Sale_and_lease_of_land' :request.GET.get('sale_and_lease_of_land',-1)

     
        }                                                                                                                                                                                                             
     
        #                                       
        tariffs = Tariffs.objects.filter(pk__in=id_tarrifs.values())
        
        result_sum = 0
        for tariff in tariffs:       

            try:           
                result_sum += int( (int(tariff.price)-(int(tariff.price)*int(tariff.shares.discount)/100) )) 
            except:
                result_sum += tariff.price 

        
        # ид трафов                  
        shp_id_tarifs = []            
        for tariff in tariffs:
            shp_id_tarifs.append(str(tariff.pk)+',')

        # ид трафов как строка 
        shp_item = ''.join(shp_id_tarifs).strip(',')            

        mrh_login = "example"        
        mrh_pass1 = "example"  

        # номер заказа                                 
        # number of order ?????????????????????                 
        inv_id = 0                                                            

        # описание заказа
        # order description
        inv_desc = "ROBOKASSA Advanced User Guide";

        #сумма заказа                           
        #sum of order         
        out_summ = result_sum              

        # предлагаемая валюта платежа
        # default payment e-currency
        in_curr = ""    

        # язык   
        # language  
        culture = "ru"                                                                         

        # формирование подписи          
        # generate signature
        # "$mrh_login:$out_summ:$inv_id:$mrh_pass1"    
        crc = md5("{}:{}:{}:{}:Shp_item={}:Shp_userid={}".format(mrh_login,out_summ,inv_id,mrh_pass1,shp_item,Shp_user_id).encode()).hexdigest()             

        template_contex = {            

           'tariffs':tariffs,
           'count_tariffs':tariffs.count(),
           'mrh_login':mrh_login,
           'mrh_pass1':mrh_pass1,
           'out_summ':out_summ,
           'crc':crc,
           'culture':culture,
           'shp_item':shp_item,
           'inv_id':inv_id,
           'in_curr':in_curr,
           'Shp_user_id':Shp_user_id,
           'inv_desc':inv_desc


        }                    

        
        return render_to_response('form/build_form_maney.html',template_contex,RequestContext(request))
    else:
        return HttpResponse('Пожалуйста зарегистрируйтесть!!!')    
             




#   result url                                                                 
def oprder_tariffs(request):                               

    #  
    mrh_pass2 = 'example'                           


    out_summ = request.GET.get('OutSum','')     
    inv_id = request.GET.get('InvId','')     
    shp_item = request.GET.get('Shp_item','')
    user_id = request.GET.get('Shp_userid','-1')    

    crc = request.GET.get('SignatureValue','').upper()                             

    my_crc = md5('{}:{}:{}:Shp_item={}:Shp_userid={}'.format(out_summ,inv_id,mrh_pass2,shp_item,user_id).encode()).hexdigest()
                           

    if my_crc.upper() != crc:                  
        return HttpResponse("bad sign\n")         
    else:      
        
        id_tarrifs = shp_item.split(',')
        tariffs = Tariffs.objects.filter(pk__in=id_tarrifs)         

        
        # обновления даты тарифов                           
        for tariff in tariffs:                                                                

            prepare = tariff.validity_date.split('=')
            date_arg = dict([(prepare[0],int(prepare[1]),)])  

            date_time_to = timezone.now()+timedelta(**date_arg)
            
            try:          

                is_taeiff = TariffsOrders.objects.get(user_id=user_id,tariff_type=tariff.tariffs_type)

                if is_taeiff.is_active:            
                    is_taeiff.date_time_to = (is_taeiff.date_time_to + timedelta(**date_arg))
                else:
                    is_taeiff.date_time_to = date_time_to 

                is_taeiff.is_active = True                
                is_taeiff.save()

                # логируем покупку    
                TariffsOrdersLogs.objects.create(user_id=user_id,tariff_type=tariff.tariffs_type,inv_id=inv_id,out_summ=tariff.price)



            except ObjectDoesNotExist:    

                TariffsOrders.objects.create(user_id=user_id,tariff_type=tariff.tariffs_type,date_time_to=date_time_to)
                # логируем покупку    
                TariffsOrdersLogs.objects.create(user_id=user_id,tariff_type=tariff.tariffs_type,inv_id=inv_id,out_summ=tariff.price)
        # обновления даты тарифов    

        ok_res = "OK{}\n".format(inv_id)

        return HttpResponse(ok_res) 



#  result success   
def tariffs_success(request):    

    #  
    # mrh_pass2 = 'u2tVBP9RWjx88eyL2MhS'   
    mrh_pass1 = "Za0nHqgon1gC8vV9mG8e"                           


    out_summ = request.GET.get('OutSum','')     
    inv_id = request.GET.get('InvId','')     
    shp_item = request.GET.get('Shp_item','')
    user_id = request.GET.get('Shp_userid','-1')    

    crc = request.GET.get('SignatureValue','').upper()                                 

    my_crc = md5('{}:{}:{}:Shp_item={}:Shp_userid={}'.format(out_summ,inv_id,mrh_pass1,shp_item,user_id).encode()).hexdigest()
                           

    if my_crc.upper() != crc:                     
        return HttpResponse("bad sign\n")     

    else:                      

        #                   
        OrdersLogs = TariffsOrdersLogs.objects.filter(user=user_id,inv_id=inv_id)

        summ_res = int(float(out_summ))                        

        template_contex = {                  

           'OrdersLogs':OrdersLogs,
           'out_summ':summ_res  

        }                                                 

        return render_to_response('tariffs_success.html',template_contex,RequestContext(request))


#    
def tariffs_get_my_log(request):     


    user_id = request.session.get('USER',False)              

    
    #                   
    OrdersLogs = TariffsOrdersLogs.objects.filter(user=user_id)  
                      
    template_contex = {                  

       'OrdersLogs':OrdersLogs 

    }                                                   

    return render_to_response('tariffs_get_my_log.html',template_contex,RequestContext(request))


#    
def get_my_tariffs(request):           


    user_id = request.session.get('USER',False)               

    
    #                   
    my_tariffs_orders = TariffsOrders.objects.filter(user=user_id,is_active=True)    
                      
    template_contex = {                  

       'my_tariffs_orders':my_tariffs_orders   

    }                                                   

    return render_to_response('get_my_tariffs.html',template_contex,RequestContext(request))



#  result fail   
def tariffs_fail(request):   

    inv_id = request.GET.get('InvId','')   

    fail_res = "Вы отказались от оплаты. Заказ{}\n".format(inv_id)

    return HttpResponse(ok_res)                  
                                   






def descriptions_tariffs(request):  

    des_tar = DescriptionsTariffs.objects.all()[0] 


    template_contex = {    

        'des_tar':des_tar       
    }            

    
    return render_to_response('tariffs_desc.html',template_contex,RequestContext(request))



def payment_methods_tariffs(request):  

    des_tar = PaymentMethodsTariffs.objects.all()[0]      


    template_contex = {      

        'des_tar':des_tar       
    }            

    
    return render_to_response('tariffs_desc.html',template_contex,RequestContext(request))



def shares_list(request):      

    shares_list = Shares.objects.all()   


    template_contex = {         

        'shares_list':shares_list       
    }            

    
    return render_to_response('shares_list.html',template_contex,RequestContext(request))                                  
        