from django.contrib import admin
from .models import *

#   
@admin.register(Pages)                    
class AdminPages(admin.ModelAdmin):
    
    class Media:      
        js = [ 
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js', '/static/grappelli/tinymce_setup/tinymce_setup.js',
            '/static/grappelli/tinymce_setup/content_typography.css'  
        ]     




