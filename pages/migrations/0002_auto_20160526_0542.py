# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pages',
            name='main_page',
            field=models.BooleanField(unique=True, verbose_name='Главная страница', default=False),
        ),
        migrations.AlterField(
            model_name='pages',
            name='descriptions',
            field=models.TextField(verbose_name='Текст страницы'),
        ),
    ]
