# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pages',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(verbose_name='Название страницы', max_length=500)),
                ('descriptions', models.TextField(blank=True, verbose_name='Текст страницы')),
            ],
            options={
                'verbose_name_plural': 'Статьи',
                'verbose_name': 'Статья',
            },
        ),
    ]
