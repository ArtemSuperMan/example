from django.db import models


# Статьи                                                                                               
class Pages(models.Model):                                                

                                                                    
    title = models.CharField(verbose_name='Название страницы',max_length=500)  

    descriptions = models.TextField(verbose_name='Текст страницы')        

    main_page = models.BooleanField(verbose_name='Главная страница',default=False,unique=True)      

    class Meta:      
        verbose_name = "Статья"        
        verbose_name_plural = "Статьи"                

    def __str__(self):  
        return  self.title      
