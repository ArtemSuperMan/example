from django.db.models import Q
from .models import *
from django.http import HttpResponse
from django.template import RequestContext  
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404

#                                                                  
def home(request):                  

    pages_list = Pages.objects.all()        
 
    curent_page = get_object_or_404(Pages,main_page=True)                      
    
    return render_to_response('views_articl.html',{"pages_list":pages_list,"curent_page":curent_page},RequestContext(request))


#                       
def article(request,page_id): 

    pages_list = Pages.objects.all()        
 
    curent_page = get_object_or_404(Pages,pk=page_id)                              
    
    return render_to_response('views_articl.html',{"pages_list":pages_list,"curent_page":curent_page},RequestContext(request))