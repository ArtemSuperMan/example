from  django.db import models  

from hashlib import md5


# юзеры                                                                                 
class Users(models.Model):                                                              
    
    # Тип юзера            
    USERS_TYPE = (                                                    

           ('UserCompany','Юр. лио',),
           ('UserIndividual','Физ. лио',),            

    )                                                                    
    type_user = models.CharField(max_length=100,choices=USERS_TYPE)
    # Тип юзера                    
    
    # Фио                                                                                          
    fio = models.CharField(max_length=100,blank=True)
    # телефон                  
    phone = models.CharField(max_length=100,blank=True)       

    # Наименования компании                            
    name_company = models.CharField(max_length=100,blank=True)    
    # Форма собственности  
    form_ownership = models.TextField(blank=True)            
    # Данные ОГРН    
    ogrn = models.CharField(max_length=100,blank=True)      
    # ИНН
    nnn = models.CharField(max_length=100,blank=True)            
    # юр адрес  
    addres = models.TextField(blank=True)  # ???????????? 
    # логин                                                                                            
    email = models.CharField(max_length=100)                
    # пароль  
    password = models.CharField(max_length=100)                        
 
    key_reset = models.CharField(max_length=50,blank=True,null=True)  
    time_reset = models.TimeField(null=True)       

    # ключ активации профеля 
    key_active = models.CharField(max_length=50,blank=True) #?????                   
    # прошел потверждения по email         
    is_active = models.BooleanField(default=0)   # ????? 


    def save(self,*args,**kwargs):   


        if len(self.password) < 32:                
            self.password = md5(str(self.password).encode()).hexdigest()     

        super(Users,self).save(*args,**kwargs)  

    #                                                   
    def __str__(self):                
        return self.fio           

