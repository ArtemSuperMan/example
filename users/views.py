from .models import * 
from realEstate.models import *
from users import add_form     
from django.template import RequestContext      
from django.core.mail import send_mail,BadHeaderError
from webApp.settings import BASE_DIR    
from django.shortcuts import render_to_response
from django.http import HttpResponse
from hashlib import md5
from datetime import datetime,timedelta  
import json
from django.shortcuts import redirect    
import threading 
import random   
from django.template.loader import render_to_string         



def random_id(length):       
    rid = ''
    for x in range(length):  
        rid += random.choice(('!@#$%^&*()_-+=')+'abcdefghijklmnopqrstuvwxyz')
    return rid


    
#   функция отправки письма в отдельном потоке                            
def send_user_mail(Subject,descriptions,text_mail,recruiter_email,email_from,auth_user,auth_password):  
                                                  
    # try:  

    send_mail(   

            Subject,
            descriptions,
            email_from,  
            recruiter_email,
            auth_user=auth_user,
            auth_password= auth_password,
            fail_silently=False,
            html_message=text_mail
        )

    # except Exception as error:                                       

    #     if str(error.args).count("Message rejected under suspicion of SPAM"):

    #         print("Яндкс банит!")         
                    


# Ркгистрация юзера         
def registration(request):                                              
 
    # Генерация ключа для подтверждения по почте           
    key_active = md5((str(datetime.now().microsecond).encode())+str('sыSф2&4пds@#@dsd').encode()).hexdigest()             
    
    type_user = request.POST.get('type_user',False)
    fio = request.POST.get('fio',False)
    phone = request.POST.get('phone',False)
    name_company = request.POST.get('name_company',False)
    form_ownership =  request.POST.get('form_ownership',False)
    ogrn =  request.POST.get('ogrn',False)
    nnn = request.POST.get('nnn',False)
    addres = request.POST.get('addres',False)  


    email = request.POST.get('email',False)       
    password = request.POST.get('password',False)  

    
    is_user_reg = Users.objects.filter(email=email)

    if len(is_user_reg):          

        return HttpResponse(json.dumps({"message":"no_reg"}))             

    else:                    

        if type_user =='UserIndividual':               
            user = Users.objects.create(type_user=type_user,fio=fio,phone=phone,key_active=key_active,email=email,password=password)
        elif type_user =='UserCompany':
            user = Users.objects.create(  
                            type_user=type_user,
                            fio=fio,phone=phone,
                            name_company=name_company,
                            form_ownership=form_ownership,
                            ogrn=ogrn,
                            nnn=nnn,
                            addres=addres,  
                            key_active=key_active,
                            email=email,
                            password=password
                   )

        user_session = Users.objects.get(key_active=key_active)    
        request.session['USER'] = user_session.pk        

        #  адреса получателей                     
        recruiter_email = [email]                           
        # 'webgutar@gmail.com'   
        subject = 'Вы успешно зарегистрировались на example.ru'  
        desc =  'Подтверждения ренистрации на example.ru'        

        context_message_reg = {                                         

            "addres_site":request.META['SERVER_NAME'],  
            "key_active":key_active,
            "fio":fio,
            "login":email,
            "password":password,

        }            

        message_reg = render_to_string('register_mail.html',context_message_reg) 

        auth_user="example@test.ru"
        auth_password="example@test.ru"       
          

        #  запуск отправки письма в одельном патоке   
        t = threading.Thread(target=send_user_mail,args=(subject,desc,message_reg,recruiter_email,auth_user,auth_user,auth_password))
        t.start()                      
        
        return HttpResponse(json.dumps({"message":"ok_reg"}))         



# Активация по почте                                          
def ectiveUserToMail(request):                    
    
    #                              
    key_active = request.GET.get('key_active',False)  

    if key_active.strip():  

        #
        try:           
            user = Users.objects.get(key_active=key_active)
            user.is_active=True
            user.key_active=''       
            user.save()                          
            request.session['USER'] = user.pk     
            return redirect('/')  
        except:   

            return HttpResponse('Ошибка ключ просрочен!')

    else:

        return HttpResponse('Ошибка ключ пустой!')            


#                
def profil(request):        

    if request.session.get('USER',False):  

        user_id = request.session.get('USER',False)

        return render_to_response('profil.html',{},RequestContext(request))

    else:

        return redirect('/') 



# формы заполнения данных                          
def add_info(request):

    try:              

        user_id = request.session.get('USER',False)
        #             
        user_info = Users.objects.get(pk=user_id)

        
        #  обычный юзер         
        if user_info.type_user=='UserIndividual':                 
            
            if request.POST:

                user_info.phone =  request.POST.get('phone')
                user_info.fio =  request.POST.get('fio')
                user_info.email =  request.POST.get('email')
                user_info.save()  
            
            contect_template = {"UserIndivision_info":user_info}
            
            return render_to_response('form_info_UserIndividual.html',contect_template,RequestContext(request))
            # обычный юзер  
        
        #  юр лицо                               
        if user_info.type_user=='UserCompany':                                                       
 
            if request.POST:

                user_info.phone = request.POST.get('phone')  
                user_info.fio =  request.POST.get('fio')
                user_info.email =  request.POST.get('email')
                user_info.name_company =  request.POST.get('name_company')
                user_info.form_ownership =  request.POST.get('form_ownership')
                user_info.ogrn =  request.POST.get('ogrn')
                user_info.nnn =  request.POST.get('nnn')
                user_info.addres =  request.POST.get('addres')      
                user_info.save()


            contect_template = {"UserIndivision_info":user_info}     

            return render_to_response('form_info_UserCompany.html',contect_template,RequestContext(request))
        #  юр лицо  





    except Exception as error:         

        return HttpResponse(error)             




def login(request):                                        

    email = request.POST.get('email','').strip()  
    password = md5(str(request.POST.get('password','').strip()).encode()).hexdigest()    

    #                   
    try:

        user = Users.objects.get(email=email,password=password)    
        
        if user.is_active:
            request.session['USER'] = user.pk         
            return HttpResponse(json.dumps({"message":"ok"}))  
        else:        
            return HttpResponse(json.dumps({"message":"er_act"}))

    except Exception as e:               
        return HttpResponse(json.dumps({"message":"error_auth"}))   



def get_user_menu_ajax(request):                     


    return render_to_response('user_menu.html',{},RequestContext(request))  



 
def reset_password(request): 

    curent_time = datetime.now()              

    # email = request.GET.get('email',False)  

    email = request.POST.get('email','').strip()                           

    gen_password = random_id(10)               
    key = md5(str(str(email)+gen_password+'@dsd!@'+str(curent_time.timestamp())).encode()).hexdigest()                                

    #                                               
    try:                                   
 
        user = Users.objects.get(email=email)                         
        user.key_reset = key                 
        user.time_reset = curent_time+timedelta(hours=2)                 
        user.save()    

        context_message_reset = {  

            "addres_site":request.META['SERVER_NAME'],  
            "key":key,
            "user_mail":email

        }    

        message_reset = render_to_string('mail_reset_pass.html',context_message_reset)  

        # текст письма                           
        subject = 'Восстановление доступа для сайта example.ru'              
        desc =  'Восстановление доступа для сайта example.ru'                                         
        recruiter_email = [email]   

        auth_user='example.ru'
        auth_password='example.ru'           

        # print(email)                                        

        #  запуск отправки письма в одельном патоке     
        t = threading.Thread(target=send_user_mail,args=(subject,desc,message_reset,recruiter_email,auth_user,auth_user,auth_password))
        t.start()             


        return HttpResponse(json.dumps({"message":"ok_reset"}))  

    
    except Exception as e:              
        return HttpResponse(json.dumps({"message":"error_reset"}))      



def reset_password_form(request): 
    
    try:
        del request.session['USER']        
    except Exception as e:
        pass                                                      
     
    key_resset,mailto = request.GET.get('key_reset',False),request.GET.get('mailto',False)

    if key_resset and mailto:

        
        contect_template = { "key_resset":key_resset,"mailto":mailto } 
        
        return render_to_response('form_reset_password.html',contect_template,RequestContext(request))     

    else:  
        return HttpResponse(json.dumps({"message":"error_reset"}))



def reset_password_action(request):        


    if  request.POST.get('key_resset',False) and  request.POST.get('mailto',False) and request.POST.get('password',False):
        
        try:   

            mailto = request.POST.get('mailto','')
            key_resset = request.POST.get('key_resset','')                        

            user = Users.objects.get(email=mailto,key_reset=key_resset)          
            user.password = md5(str(request.POST.get('password','').strip()).encode()).hexdigest()
            user.save()

            request.session['USER'] = user.pk                    

            return redirect('/')          

            # return HttpResponse(json.dumps({"message":"ok_reset"}))                      

        except Exception as e:                
            return HttpResponse(json.dumps({"message":"error_reset"}))        

    else:     
        return redirect('/')             







def layout(request):          

    try:  
        del request.session['USER']    
        return redirect('/')
    except: 
        return redirect('/')         

