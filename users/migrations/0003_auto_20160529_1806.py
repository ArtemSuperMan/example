# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20160415_1626'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='key_reset',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='users',
            name='time_reset',
            field=models.TimeField(null=True),
        ),
    ]
