# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='balance',
        ),
        migrations.AlterField(
            model_name='users',
            name='key_active',
            field=models.CharField(max_length=50, blank=True),
        ),
    ]
