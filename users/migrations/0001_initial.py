# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('type_user', models.CharField(max_length=100, choices=[('UserCompany', 'Юр. лио'), ('UserIndividual', 'Физ. лио')])),
                ('fio', models.CharField(max_length=100, blank=True)),
                ('phone', models.CharField(max_length=100, blank=True)),
                ('name_company', models.CharField(max_length=100, blank=True)),
                ('form_ownership', models.TextField(blank=True)),
                ('ogrn', models.CharField(max_length=100, blank=True)),
                ('nnn', models.CharField(max_length=100, blank=True)),
                ('addres', models.TextField(blank=True)),
                ('email', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('balance', models.CharField(max_length=100, blank=True)),
                ('key_active', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=0)),
            ],
        ),
    ]
