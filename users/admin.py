from django.contrib import admin
from .models import *   

# 
@admin.register(Users)                    
class AdminUsers(admin.ModelAdmin):                         
	list_display = ('pk','type_user','fio','email','is_active',)

