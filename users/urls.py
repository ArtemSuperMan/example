from django.conf.urls import url
from . import views


#
urlpatterns = [                  
    
    url(r'^registration/$', views.registration),
    url(r'^ectivemail/$', views.ectiveUserToMail),    
    url(r'^$', views.profil),
    url(r'^addinfo/$', views.add_info),
    url(r'^login/$', views.login),
    url(r'^layout/$', views.layout),
    url(r'^menu/$', views.get_user_menu_ajax),
    url(r'^reset/password/$', views.reset_password),
    url(r'^reset/password/user/key/$', views.reset_password_form),
    url(r'^reset/actions/$', views.reset_password_action)                
        
]
