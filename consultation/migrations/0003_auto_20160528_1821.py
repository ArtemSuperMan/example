# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consultation', '0002_auto_20160523_2031'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='сonsultants',
            options={'verbose_name_plural': 'Консультанты', 'verbose_name': 'Консультант'},
        ),
    ]
