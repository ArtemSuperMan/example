# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Сonsultants',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('type_consultants', models.CharField(max_length=100, choices=[('Lawyer', 'Консультант-Юрист'), ('Сonsultant', 'Консультант по общим вопросам')])),
                ('fio', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('totla_question', models.IntegerField(blank=True, default=0)),
            ],
        ),
    ]
