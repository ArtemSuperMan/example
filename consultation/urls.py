from django.conf.urls import url
from . import views


#
urlpatterns = [        

    url(r'^filter/$', views.filter),
    url(r'^getform/$', views.get_add_form_filter),
    url(r'^getres/$', views.ajax_filter),
    url(r'^addadvt/$', views.add_advt),                 
]  
