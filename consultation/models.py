from django.db import models      


# консультанты                                                                               
class Сonsultants(models.Model):                                                                        
    
    # Тип консультанта                                     
    СONSULTANT_TYPE = (                                                            

           ('Lawyer','Консультант-Юрист',),      
           ('Сonsultant','Консультант по общим вопросам',),               

    )                                                                    
    type_consultants = models.CharField(max_length=100,choices=СONSULTANT_TYPE)  
    # Тип консультанта                      
    # Фио                                                                                               
    fio = models.CharField(max_length=100)         
    # телефон                  
    phone = models.CharField(max_length=100)   
    # email                                                                                            
    email = models.CharField(max_length=100,unique=True)      
    # количество вопросов (для конкуренции)
    totla_question = models.IntegerField(blank=True,default=0)


    class Meta:      
        verbose_name = "Консультант"              
        verbose_name_plural = "Консультанты"                               

    #                                                   
    def __str__(self):                
        return self.fio  
