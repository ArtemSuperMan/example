from django.db import models


# Статьи                                                                                               
class StopWorlds(models.Model):                                                   

                                                                    
    title = models.CharField(verbose_name='Название страницы',max_length=500)  

      
    class Meta:      
        verbose_name = "Стоп слово"        
        verbose_name_plural = "Стоп слова"                

    def __str__(self):  
        return  self.title        