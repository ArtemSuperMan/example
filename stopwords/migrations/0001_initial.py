# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StopWorlds',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(verbose_name='Название страницы', max_length=500)),
            ],
            options={
                'verbose_name_plural': 'Стоп слова',
                'verbose_name': 'Стоп слово',
            },
        ),
    ]
