from realEstate.models import *
from tariffs.models import *
from users.models import *      
from django.template.loader import render_to_string 
from django.utils import timezone 
from advertising.models import * 
from django.core.context_processors import csrf      

#                 
def apps_context(request):   


    AdvertisingBlog.objects.filter(date_time_to__lt=timezone.now()).update(is_active=False)
    
    AdvertisingFilter.objects.filter(date_time_to__lt=timezone.now()).update(is_active=False)

                   
    #   
    context_modal_reg = {}
    context_modal_reg.update(csrf(request))
    
    modal_reg = render_to_string('modal_reg.html',context_modal_reg)            

    modal_mortgage = render_to_string('modal_mortgage.html')                             
    
    #  
    user_id = request.session.get('USER',False)
    user_info = None 

    if user_id:          
        
        # запрос к базе на деактивацию тарифа юзверя 
        TariffsOrders.objects.filter(user_id=user_id,date_time_to__lt=timezone.now()).update(is_active=False)

        try:
            user_info = Users.objects.get(pk=user_id)
        except:
            pass       

             
    menu  = None      
    type_transaction = None       


                             
    try:       
        menu = realtObject.OJECTS_TYPE
        type_transaction = realtObject.TRANSACTION_TYPE  
    except:
        pass 

    # print(user_info)                   

    return {'modal_mortgage':modal_mortgage,'modal_reg':modal_reg,'user_info':user_info,'menu':menu,"type_transaction":type_transaction}         
