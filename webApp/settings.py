# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os    

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!ytckmp@1qod$5!*9_u$k3$7##tpi+370zc6)750nw1n$ex&j)'   

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True     
ALLOWED_HOSTS = []       

# DEBUG = False   
# ALLOWED_HOSTS = ['sobstvennik23.ru',]  
      

# Application definition

INSTALLED_APPS = (       
    
    'grappelli',
    'filebrowser',    
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'daterange_filter',
    'realEstate',
    'users',
    'consultation',
    'blog',
    'tariffs',
    'pages',
    'homeImages',
    'stopwords',  
    'mortgage',
    'advertising',
    # 'yandex_money',             
)     

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',      
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'webApp.urls'   

# # настройка шаблонизатора    
TEMPLATES = [      
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',

        'APP_DIRS': True, # нехрена не понятно надо разобраться       

        'DIRS': [                           

            os.path.join(BASE_DIR,'templates'),    
            os.path.join(BASE_DIR,'realEstate/realEstate_layout'), # шаблон из приложения realEstate 
            os.path.join(BASE_DIR,'users/users_layout'), # шаблон из приложения users
            os.path.join(BASE_DIR,'blog/blog_layout'), # шаблон из приложения blog
            os.path.join(BASE_DIR,'tariffs/tariffs_layout'), # шаблон из приложения tariffs
            os.path.join(BASE_DIR,'consultation/consultation_layout'), # шаблон из приложения tariffs
            os.path.join(BASE_DIR,'pages/pages_layout'), # шаблон из приложения pages 
            os.path.join(BASE_DIR,'mortgage/mortgage_layout') # шаблон из приложения pages 
                        
        ],    

        'OPTIONS': {    
            'context_processors': [          
                'django.template.context_processors.debug',  
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # 'users.context_processors.preprocess_context',
                'context_processors.context_processors.apps_context'       
                # !!! добавили в конце функцию-препроцессор из нашего приложения     
            ],  
        },
    },
]  

#  
WSGI_APPLICATION = 'webApp.wsgi.application'  


# Database     
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }   

DATABASES = {      
    'default': {   
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'app_example',
        'USER': 'app_example',
        'PASSWORD': 'app_example',  
        'HOST': '127.0.0.1',  
        'PORT': '3306',      
    'OPTIONS': {   
          'autocommit': True,
        },
    }        
} 


    
# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True   

USE_L10N = True  

USE_TZ = True           


# Static files (CSS, JavaScript, Images)     
# https://docs.djangoproject.com/en/1.8/howto/static-files/


# # путь до папки media, в общем случае она пуста в начале   
MEDIA_ROOT = os.path.join(BASE_DIR,'media')
MEDIA_URL = '/media/'  # URL для медии в шаблонах       

# путь до главной статики + django статики 
STATIC_ROOT = os.path.join(BASE_DIR,'static')              
# урл для всех приложений  до статики   
STATIC_URL = '/static/' 


#  ??????????????????                       
STATICFILES_DIRS = (      

    # os.path.join(BASE_DIR,'static/tinymce'),  
    os.path.join(BASE_DIR,'static/development'),  
    # os.path.join(BASE_DIR,'static/grappelli'),
    # os.path.join(BASE_DIR,'static/filebrowser'),  

)          

# from filebrowser.sites import site

# site.directory = "uploads/"
FILEBROWSER_DIRECTORY =  "uploads/"    


# yandex                                            
SERVER_EMAIL = 'smtp.yandex.ru'                
EMAIL_HOST = 'smtp.yandex.ru'                          
EMAIL_FROM = 'example@example.ru'   
EMAIL_HOST_USER = 'example'   
EMAIL_HOST_PASSWORD = 'example'
EMAIL_USE_TLS = True            
EMAIL_PORT = 587       
    