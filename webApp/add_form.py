from django.forms import ModelForm  
from realEstate.models import realtObject
from users.models import *    


class RealtObjectForm(ModelForm):  


    class Meta:        
        model = realtObject
        fields = '__all__'

            