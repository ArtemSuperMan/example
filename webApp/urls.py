#! /usr/bin/env python3

import os  

from django.conf import settings

from django.conf.urls import include, url
from filebrowser.sites import site      
from django.contrib import admin    
from realEstate import  views as views_realEstate ,urls as urls_realEstate
from users import  urls as urls_users
from blog import  urls as urls_blog 
from tariffs import  urls as urls_tariffs
from consultation import  views 
from pages import  urls as urls_pages    


site.directory = "filebrowser/"    

urlpatterns = [                             
   

    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/filebrowser/', include(site.urls)),     
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views_realEstate.home),   
    url(r'^user/', include(urls_users)),
    url(r'^blog/', include(urls_blog)),
    #url(r'^tariffs/', include(urls_tariffs)),       
    url(r'^realt/', include(urls_realEstate)),
    url(r'^consultation/',views.feed_back),
    url(r'^pages/',include(urls_pages)),              
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}), # !!!             

    # url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  {'document_root':  settings.MEDIA_ROOT }), # !!!
    # url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }), # !!!
    
    
   ]   
