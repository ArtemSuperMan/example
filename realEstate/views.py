from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger  
from django.db.models import Q
from django.http import HttpResponse
from django.template import RequestContext  
from realEstate.models import *       
from users.models import *
from tariffs.models import *
from django.shortcuts import render_to_response  
from django.core import serializers
import json
from . import add_form 
from django.shortcuts import redirect    
from .my_filter import MyFilter
from django.template.loader import render_to_string 
from homeImages.models import homeImage
from django.utils import formats
from stopwords.models import StopWorlds  
from advertising.models import * 

import time   
              

#          
def home(request): 

             
    
    Сonsultant_html = render_to_string('Сonsultant.html',{})        
    Lawyer_html = render_to_string('Lawyer.html',{}) 

    home_image = homeImage.objects.first()                      

 
    return render_to_response('home.html',{"home_image":home_image,"Сonsultant_html":Сonsultant_html,"Lawyer_html":Lawyer_html},RequestContext(request)) 


#                    
def get_add_form_filter(request):    

    form = add_form.RealtObjectForm()                      
    
    swich_filter = request.GET.get('swich_filter',False)                    

    if swich_filter:                                  
    
        if swich_filter =='Room':                                                                                                                                                 
            return render_to_response('form/filter_form/filter_room.html',{"form":form},RequestContext(request))

        if swich_filter =='Apartment':                                                                                                                                         
            return render_to_response('form/filter_form/filter_Apartment.html',{"form":form},RequestContext(request))

        if swich_filter =='HouseDachaCottage':                                                                                                                                           
            return render_to_response('form/filter_form/filter_HouseDachaCottage.html',{"form":form},RequestContext(request))

        if swich_filter =='Production':                                                                                                                                           
            return render_to_response('form/filter_form/filter_ProductionReadyBusiness.html',{"form":form},RequestContext(request))

        if swich_filter =='FreeAssignment':                                                                                                                                           
            return render_to_response('form/filter_form/filter_FreeAssignment.html',{"form":form},RequestContext(request))

        if swich_filter =='ReadyBusiness':                                                                                                                                           
            return render_to_response('form/filter_form/filter_ProductionReadyBusiness.html',{"form":form},RequestContext(request))

        if swich_filter =='Stock':                                                                                                                                           
            return render_to_response('form/filter_form/filter_Stock.html',{"form":form},RequestContext(request))

        if swich_filter =='GaragesParkingLots':                                                                                                                                           
            return render_to_response('form/filter_form/filter_GaragesParkingLots.html',{"form":form},RequestContext(request))                     

        if swich_filter =='Land':                                                                                                                                             
            return render_to_response('form/filter_form/filter_Land.html',{"form":form},RequestContext(request))                     


    else:            
        return HttpResponse('Выберите тип недвижимости!')                

    

           
def filter(request):                                                                                                                              
    
    type_transaction = request.GET.get('type_transaction',False) 

    micro_district = Microdistrict.objects.all()  

    try:
        advertising_Left  = AdvertisingFilter.objects.get(is_active=True,position='left')
    except:
        advertising_Left = False  

    try:
        advertising_right = AdvertisingFilter.objects.get(is_active=True,position='right') 
    except:
        advertising_right = False         


    #                                       
    template_content = {                  

        "type_transaction":type_transaction,
        "micro_district":micro_district,
        "advertising_Left":advertising_Left,
        "advertising_right":advertising_right          
    } 


    return render_to_response('filter.html',template_content,RequestContext(request))



#          
def ajax_filter(request):        


    user_session_id = request.session.get('USER',False)                                                                                                                             

                                             
    fields_one_input = [                      

        {'type_transaction':request.GET.get('type_transaction','') },    
        {'type_house':request.GET.get('type_house','') },
        {'stay_in':request.GET.get('stay_in','') },
        {'number_rooms':request.GET.get('number_rooms','') },
        {'pepairs':request.GET.get('pepairs','') },
        {'presence_furniture':request.GET.get('presence_furniture','') },
        {'toilet':request.GET.get('toilet','') },
        {'category':request.GET.get('category','') },  
        {'category_land':request.GET.get('category_land','') },
        {'security':request.GET.get('security','')},
        {'banya':request.GET.get('banya','')},
        {'garage':request.GET.get('garage','')},
        {'pool':request.GET.get('pool','')},
        {'outbuildings':request.GET.get('outbuildings','')},
        {'electricity':request.GET.get('electricity','')},
        {'heating':request.GET.get('heating','')},
        {'sewage':request.GET.get('sewage','')},
        {'water_supply':request.GET.get('water_supply','')},
        {'class_structure':request.GET.get('class_structure','')},
        {'garage_type':request.GET.get('garage_type','')},  
        {'type_parking_spaces':request.GET.get('type_parking_spaces','')},
        {'type_structure':request.GET.get('type_structure','')},
        {'type_commerce':request.GET.get('type_commerce','')},
                    
                
        
    ]    

    #  поля одного выбора             
    Q_fields_one_input = Q()                          
    for field in  fields_one_input:
        if not '' in field.values():    
            Q_fields_one_input &= Q(**field) 


    # тип объекта 
    type_objects = request.GET.getlist('type_object',[''])
    #print(type_objects)       
    #GaragesParkingLots 
    Q_type_object = Q()            
    if not '' in  type_objects:    

        if 'GaragesParkingLots' in type_objects and len(type_objects)==1:  
            type_objects =  ['Garages','Parking_spaces']  
   
        if 'HouseDachaCottage' in type_objects and len(type_objects)==1:  
            type_objects =  ['HouseDachaCottage', 'House', 'Dacha', 'Part_of_house', 'Apart_of_house', 'Vremyanka', 'Townhouse', 'Duplex', 'Cottage']
        
        #print(type_objects)
        for type_object_name in  type_objects:  
            Q_type_object |= Q(type_object=type_object_name)
    else:
        Q_type_object = Q()  
                                                               
    
    # Бытовая техника                                                         
    Q_appliances = Q()                
    appliances = request.GET.getlist('appliances',False)
    if appliances:
        Q_appliances &= Q(appliances__in=appliances)                      

    # Q_appliances &= Q(  Q(appliances=3) & Q(appliances=4) & Q(appliances=5)   )                             

     # Бытовая техника                                            

    # #print(request.GET.getlist('appliances',list('')))         

    #  микра районы          
    Q_microdistricts = Q()       
    microdistricts = request.GET.getlist('microdistrict[]','')   
    for microdistrict_pk in  microdistricts:
        Q_microdistricts |= Q(microdistrict=microdistrict_pk)

    #Лоджия (количество)
    Q_loggia_number  = Q()         
    loggia_number = request.GET.getlist('loggia_number','')   
    for loggia_number_pk in  loggia_number:
        Q_loggia_number |= Q(loggia_number=loggia_number_pk)

    #Балкон (количество)
    Q_balcony_number  = Q()       
    balcony_number = request.GET.getlist('balcony_number','')   
    for balcony_number_pk in  balcony_number:
        Q_balcony_number |= Q(balcony_number=balcony_number_pk)     

    # общая площадь     
    total_area_renge = [ request.GET.get('total_area_from',False),request.GET.get('total_area_to',False)]
    Q_total_area = Q()
    if  total_area_renge[0] or total_area_renge[1]:
        if not total_area_renge[0]:
            total_area_renge[0] = 0  
        if  not total_area_renge[1]:  
            total_area_renge[1] = 200000000
        Q_total_area &= Q(total_area__range=total_area_renge)
    # цена      
    price_renge = [ request.GET.get('price_from',False),request.GET.get('price_to',False)]
    Q_price_renge = Q()
    if  price_renge[0] or price_renge[1]:  
        if not price_renge[0]:
            price_renge[0] = 0  
        if  not price_renge[1]:  
            price_renge[1] = 200000000  
        Q_total_area &= Q(price__range=price_renge)  

    land_area_renge = [ request.GET.get('land_area_from',False),request.GET.get('land_area_to',False)]
    Q_land_area = Q()     
    if  land_area_renge[0] or land_area_renge[1]:
        if not land_area_renge[0]:
            land_area_renge[0] = 0  
        if  not land_area_renge[1]:  
            land_area_renge[1] = 200000000
        Q_land_area &= Q(land_area__range=land_area_renge)
    # Расстояние до города км
    distance_town_renge = [ request.GET.get('distance_town_from',False),request.GET.get('distance_town_to',False)]
    Q_distance_town = Q()     
    if  distance_town_renge[0] or distance_town_renge[1]:
        if not distance_town_renge[0]:
            distance_town_renge[0] = 0  
        if  not distance_town_renge[1]:     
            distance_town_renge[1] = 200000000
        Q_distance_town &= Q(distance_town__range=distance_town_renge)
    # этаж 
    floor_renge = [ request.GET.get('floor_from',False),request.GET.get('floor_to',False)]
    Q_floor_renge = Q()     
    if  floor_renge[0] or floor_renge[1]:    
        if not floor_renge[0]:
            floor_renge[0] = 0  
        if  not floor_renge[1]:     
            floor_renge[1] = 200000000
        Q_floor_renge &= Q(floor__range=floor_renge)

    # Этажность 
    storey_renge = [ request.GET.get('storey_from',False),request.GET.get('storey_to',False)]
    Q_storey_renge = Q()     
    if  storey_renge[0] or storey_renge[1]:    
        if not storey_renge[0]:
            storey_renge[0] = 0  
        if  not storey_renge[1]:     
            storey_renge[1] = 200000000
        Q_storey_renge &= Q(storey__range=storey_renge)


     # Площадь Жилая 
    living_area_renge = [ request.GET.get('living_area_from',False),request.GET.get('living_area_to',False)]
    Q_living_area_renge = Q()     
    if  living_area_renge[0] or living_area_renge[1]:    
        if not living_area_renge[0]:
            living_area_renge[0] = 0  
        if  not living_area_renge[1]:     
            living_area_renge[1] = 200000000
        Q_living_area_renge &= Q(living_area__range=living_area_renge) 

    # Площадь Кухня 
    kitchen_area_renge = [ request.GET.get('kitchen_area_from',False),request.GET.get('kitchen_area_to',False)]
    Q_kitchen_area_renge = Q()     
    if  kitchen_area_renge[0] or kitchen_area_renge[1]:    
        if not kitchen_area_renge[0]:
            kitchen_area_renge[0] = 0  
        if  not kitchen_area_renge[1]:     
            kitchen_area_renge[1] = 200000000   
        Q_kitchen_area_renge &= Q(kitchen_area__range=kitchen_area_renge)

    # Залог 
    deposit_renge = [ request.GET.get('deposit_from',False),request.GET.get('deposit_to',False)]

    # #print(deposit_renge)  
    Q_deposit_renge = Q()     
    if  deposit_renge[0] or deposit_renge[1]:    
        if not deposit_renge[0]:
            deposit_renge[0] = 0  
        if  not deposit_renge[1]:     
            deposit_renge[1] = 200000000   
        Q_deposit_renge &= Q(deposit__range=deposit_renge)                                           
      


    Q_result = Q(

            Q_deposit_renge & Q_kitchen_area_renge & Q_living_area_renge & Q_storey_renge & Q_floor_renge  
            & Q_type_object & Q_fields_one_input & Q_microdistricts & Q_total_area & Q_price_renge 
            & Q_appliances & Q_land_area & Q_distance_town & Q_loggia_number & Q_balcony_number
            & Q(is_public=True)    
        )           


    # #print(fields_many_values_ForeignKey)                   
   

    ThisPage = int(request.GET.get('page',1))  
    #                                  
    count_rows = 10               
    #
    num_rows = (ThisPage * count_rows)                 
    #                                            
    start = ( (ThisPage * count_rows)-(count_rows) )  
    
    #                           
    result_count = realtObject.objects.filter(Q_result).distinct().count()    

    #                                   
    realt_objects = realtObject.objects.filter(Q_result).distinct().order_by('-add_date_time')[start:num_rows]               
    # #print(realt_objects)       
    #    
    CountPages = int(((result_count-1)/count_rows)+1)                                          

    # пять строниц с левой стороны                 
    LeftBarPage = []                       
    i1=1        
    while(((ThisPage)-i1)>0):
              LeftBarPage.append( ((ThisPage)-i1) )
              if(i1==5):              
                  break
              i1 += 1
    # пять строниц с правой стороны   
    RightBarPage = []
    i=1   
    while(((ThisPage)+i)<=CountPages):  
          RightBarPage.append( ((ThisPage)+i) )
          if(i1==5):              
              break  
          i += 1  


    # формирования объявления     
    realt_template = []                                                                     
    for objects_r in realt_objects:             
 
        realt_images = list(RealtImages.objects.filter(realt_object=objects_r.pk).values('image'))    
        
        # адрес контакты  
        try:            

            # if user_session_id:                    

            #     is_taeiff_active = TariffsOrders.objects.filter(user_id=user_session_id,tariff_type=objects_r.tarif_type,is_active=True)

            #     if is_taeiff_active:
                    
            #         if objects_r.owner == 'user':
            #             user_informer = Users.objects.get(pk=objects_r.user.pk)    
            #             informer = {"public":True,"user_phone":user_informer.phone,"user_fio":user_informer.fio}
            #         elif objects_r.owner == 'admin':  
            #             informer = {"public":True,"user_phone":objects_r.phone,"user_fio":objects_r.fio}                       
                    
            #     else:             
            #         message = "У Вас нет тарифа-{}".format(Tariffs.choices_values[objects_r.tarif_type])                
            #         informer = {"public":False,"message":message}           
            # else:    
            #     message = 'Пожалуйста зарегистрируйтесь или авторизируйтесь!'                
            #     informer = {"public":False,"message":message}   

            #  открытый доступ 
            if objects_r.owner == 'user':
                user_informer = Users.objects.get(pk=objects_r.user.pk)    
                informer = {"public":True,"user_phone":user_informer.phone,"user_fio":user_informer.fio}
            elif objects_r.owner == 'admin':  
                informer = {"public":True,"user_phone":objects_r.phone,"user_fio":objects_r.fio}                

        except:  
            informer = {"public":False,"message":'Ошибка!'}
 
        # адрес контакты       

             
        # формирования json шаблона 
        realt_template.append({      

            #      
            "pk":objects_r.pk,      
            "type_object_title":realtObject.choices_values.get(objects_r.type_object),
            "type_object":objects_r.type_object,  
            "type_transaction_title":realtObject.choices_values.get(objects_r.type_transaction),
            "type_transaction":objects_r.type_transaction,
            "deadline":str(objects_r.deadline),       
            "images":realt_images,
            "count_images":len(realt_images),  
            "add_date_time":formats.date_format(objects_r.add_date_time,"SHORT_DATETIME_FORMAT"),  
            "informer":informer,
            "price":str(objects_r.price),  
            "descriptions_short":objects_r.descriptions[:254],                   
            "descriptions_full":objects_r.descriptions,
            "showe_btn":True if (len(objects_r.descriptions) > 254) else False,
            "stay_in":str(objects_r.stay_in),
            "number_rooms":str(objects_r.number_rooms),
            "street":objects_r.street,
            "house":objects_r.house,    
            "apartment":objects_r.apartment,   
            "total_area":str(objects_r.total_area),  
            "size_room":str(objects_r.size_room),  
            "bargain":objects_r.bargain,
            "deposit":objects_r.deposit,
            "floor":objects_r.floor,
            "storey":objects_r.storey,
            "type_house":str(objects_r.type_house),
            "loggia_number":str(objects_r.loggia_number),
            "balcony_number":str(objects_r.balcony_number),
            "pepairs":str(objects_r.pepairs),
            "year_construction":objects_r.year_construction,
            "output_window":str(objects_r.output_window),
            "toilet":str(objects_r.toilet),
            "presence_furniture":str(objects_r.presence_furniture),
            "appliances":list(objects_r.appliances.all().values('title')),
            "living_area":objects_r.living_area,
            "kitchen_area":objects_r.kitchen_area,
            "land_area":objects_r.land_area,
            "electricity":str(objects_r.electricity),
            "heating":str(objects_r.heating),
            "water_supply":str(objects_r.water_supply),
            "security":objects_r.security,
            "banya":objects_r.banya,
            "pool":objects_r.pool,
            "outbuildings":objects_r.outbuildings,
            "category_land":str(objects_r.category_land),
            "cadastral_number":objects_r.cadastral_number,
            "distance_town":objects_r.distance_town,
            "sewage":str(objects_r.sewage),
            "garage_type":str(objects_r.garage_type),
            "type_parking_spaces":str(objects_r.type_parking_spaces),
            "denomination":objects_r.denomination,
            "room_condition":str(objects_r.room_condition),
            "category":str(objects_r.category),
            "type_structure":str(objects_r.type_structure),
            "class_structure":str(objects_r.class_structure),
            "building_condition":str(objects_r.building_condition),
            "ceiling_height":objects_r.ceiling_height,
            "elevator":objects_r.elevator,
            "parking":objects_r.parking,
            "internet":objects_r.internet,
            "microdistrict":str(objects_r.microdistrict)                                        
                             

        })   
        # формирования json шаблона  

    # формирования объявления 


    response_json = {             

        "items":realt_template,      
        "ThisPage":ThisPage,
        "CountPages":CountPages,      
        "LeftBarPage":LeftBarPage,    
        "RightBarPage":RightBarPage        
    }                                                                              
 

    return HttpResponse(json.dumps(response_json,ensure_ascii=False),content_type="application/json")


#   размистить объявлеия                      
def add_advt(request):                                                                
    
    form = add_form.RealtObjectForm(request.POST,request.FILES)

    # запись объявления
    if request.POST:

        user_session_id = request.session.get('USER',False)

        if user_session_id:

            user = Users.objects.filter(pk=user_session_id)[0] 

            if user.type_user != 'UserCompany':      
                                    
 
                if form.is_valid():  

                    is_StopWorlds = False      

                    descriptions =  request.POST.get('descriptions','')

                    if descriptions:    
                        
                        for world in StopWorlds.objects.all():
    
                            if descriptions.lower().find(str(world.title)) >=0:
                                is_StopWorlds = True
                                break   

                    
                    realt_obj = form.save()                 
                    realt_obj.user_id=int(request.session.get('USER',False))
                    realt_obj.is_stop_worlds = is_StopWorlds    
                    realt_obj.save()                                     
                     
                    #             
                    if request.FILES.getlist('images[]',False):         

                        for fileImege in request.FILES.getlist('images[]',False):      
                            RealtImages.objects.create(realt_object_id=realt_obj.pk,image=fileImege,index_image='1')
                            # #print(realt_obj.pk)

                    if not user.is_active:      
                        return HttpResponse(json.dumps({"message":"restricted_add"}))

                    elif user.is_active:                          
                        return HttpResponse(json.dumps({"message":"ok_add"}))

                    # if user.type_user ='' and   user_type_error                                                                                     
                
                else:      

                    # #print(form.errors)   user_type_error      
                    return HttpResponse(json.dumps({"message":"Ошибка не добавлено!"}))

            else:        
                return HttpResponse(json.dumps({"message":"user_type_error"}))  
        else:        
            return HttpResponse(json.dumps({"message":"error_auth"}))          
    # запись объявления                

 
    form_swih = request.GET.get('swich_form',False)                     

    if form_swih:                              
    
        if form_swih =='Room':                                                                                                                    
            return render_to_response('form/objects_form/add_room.html',{"form":form},RequestContext(request))

        elif form_swih =='Apartment':    
            return render_to_response('form/objects_form/add_Apartment.html',{"form":form},RequestContext(request))

        elif form_swih =='HouseDachaCottage':                                    
            return render_to_response('form/objects_form/add_HouseDachaCottage.html',{"form":form},RequestContext(request))

        elif form_swih =='Production':                                    
            return render_to_response('form/objects_form/add_Production.html',{"form":form},RequestContext(request))

        elif form_swih =='Stock':                                    
            return render_to_response('form/objects_form/add_Stock.html',{"form":form},RequestContext(request))
            
        elif form_swih =='FreeAssignment':                                    
            return render_to_response('form/objects_form/add_FreeAssignment.html',{"form":form},RequestContext(request))

        elif form_swih =='ReadyBusiness':                                    
            return render_to_response('form/objects_form/add_ReadyBusiness.html',{"form":form},RequestContext(request))           

        elif form_swih =='GaragesParkingLots':                                    
            return render_to_response('form/objects_form/add_GaragesParkingLots.html',{"form":form},RequestContext(request))     

        elif form_swih =='Land':                                    
            return render_to_response('form/objects_form/add_Land.html',{"form":form},RequestContext(request))        

    else:            
        return render_to_response('form/objects_form/default_form.html',{"form":form},RequestContext(request))



def my_add_eddit(request,id_add):   
                       

    #      
    realt_obj = realtObject.objects.get(pk=id_add)   

    is_Appliances = realt_obj.appliances.all()          

    not_Appliances = Appliances.objects.exclude(pk__in=is_Appliances)    
    
     

    try:                                       
        realt_object = realtObject.objects.get(pk=id_add)      
        form_info = add_form.RealtObjectForm(instance=realt_object,data=request.POST)
    except:
        form_info = add_form.RealtObjectForm(request.POST) 
        realt_object = form_info.data            
    
    if request.POST:                    

        if  form_info.is_valid():    
            
            is_StopWorlds = False      

            descriptions =  request.POST.get('descriptions','')

            if descriptions:    
                
                for world in StopWorlds.objects.all():

                    if descriptions.lower().find(str(world.title)) >=0:
                        is_StopWorlds = True
                        break   

            realt_obj = form_info.save()
            realt_obj.user_id=int(request.session.get('USER',False))
            realt_obj.is_stop_worlds = is_StopWorlds  
            realt_obj.save()     

            #  
            RealtImages.objects.filter(realt_object_id=realt_obj.pk).delete()

            if request.FILES.getlist('images[]',False):  

                for fileImege in request.FILES.getlist('images[]',False):      
                    RealtImages.objects.create(realt_object_id=realt_obj.pk,image=fileImege,index_image='1')
                    # #print(realt_obj.pk)

            return HttpResponse(json.dumps({"message":"ok_eddit"}))                                                                                                       
        else:
            pass                  
            # #print(form_info.errors)  ?????               
        
    HouseDachaCottage = ['House','Dacha','Part_of_house','Apart_of_house','Vremyanka','Townhouse','Duplex','Cottage']                                                      
    if realt_obj.type_object:                                
    
        if realt_obj.type_object =='Room':                                                                                                                              
            return render_to_response('form/eddit_form/eddit_room.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))

        elif realt_obj.type_object =='Apartment':      
            return render_to_response('form/eddit_form/eddit_Apartment.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))

        elif realt_obj.type_object in HouseDachaCottage:                                
            return render_to_response('form/eddit_form/eddit_HouseDachaCottage.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))    

        elif realt_obj.type_object =='Production':                                        
            return render_to_response('form/eddit_form/eddit_Production.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))

        elif realt_obj.type_object =='Stock':                                             
            return render_to_response('form/eddit_form/eddit_Stock.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))
        
        elif realt_obj.type_object =='FreeAssignment':                                               
            return render_to_response('form/eddit_form/eddit_FreeAssignment.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))

        elif realt_obj.type_object =='ReadyBusiness':                                                 
            return render_to_response('form/eddit_form/eddit_ReadyBusiness.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))
        
        elif realt_obj.type_object =='Garages':                                                        
            return render_to_response('form/eddit_form/eddit_Garages.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))
        
        elif realt_obj.type_object =='Parking_spaces':                                                                    
            return render_to_response('form/eddit_form/eddit_Parking_spaces.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))
        
        elif realt_obj.type_object =='Land':                                                                        
            return render_to_response('form/eddit_form/eddit_Land.html',{"is_Appliances":is_Appliances,"not_Appliances":not_Appliances,"realt_obj":realt_obj,"form":form_info,"realt_object":realt_object},RequestContext(request))
        
    else: 
        return HttpResponse('my_add_list.html',RequestContext(request))          


                             

def my_add_list_nome(request):        

    user_session_id = request.session.get('USER',False)    

    if not user_session_id:    
        return redirect('/')        


    return render_to_response('my_add_list.html',{},RequestContext(request))




####################################################################################  
def get_my_add_list_json(request): 
  

    user_session_id = request.session.get('USER',False)

    if not user_session_id:    
        return HttpResponse(json.dumps({"error":"auth"},ensure_ascii=False),content_type="application/json")


    user = Users.objects.filter(pk=user_session_id)[0]        

    
    ThisPage = int(request.GET.get('page',1))  
    #                                  
    count_rows = 10                   
    #
    num_rows = (ThisPage * count_rows)                 
    #                                            
    start = ( (ThisPage * count_rows)-(count_rows) )  

    #                           
    result_count = realtObject.objects.filter(user=user_session_id).count()    

    #                                   
    realt_objects = realtObject.objects.filter(user=user_session_id).order_by('-add_date_time')[start:num_rows]               
    # #print(realt_objects)       
    #    
    CountPages = int(((result_count-1)/count_rows)+1)                                          

    # пять строниц с левой стороны                 
    LeftBarPage = []                       
    i1=1        
    while(((ThisPage)-i1)>0):
              LeftBarPage.append( ((ThisPage)-i1) )
              if(i1==5):              
                  break
              i1 += 1
    # пять строниц с правой стороны   
    RightBarPage = []
    i=1   
    while(((ThisPage)+i)<=CountPages):  
          RightBarPage.append( ((ThisPage)+i) )
          if(i1==5):              
              break  
          i += 1  
             

    # формирования объявления     
    realt_template = []                                                                     
    for objects_r in realt_objects:             
 
        realt_images = list(RealtImages.objects.filter(realt_object=objects_r.pk).values('image'))    
        
        # формирования json шаблона 
        realt_template.append({      

            #      
            "pk":objects_r.pk,    
            "is_public":objects_r.is_public,
            
            "reasons_blocking":str(objects_r.reasons_blocking),

            "type_object_title":realtObject.choices_values.get(objects_r.type_object),
            "type_object":objects_r.type_object,  
            "type_transaction_title":realtObject.choices_values.get(objects_r.type_transaction),
            "type_transaction":objects_r.type_transaction,
            "deadline":str(objects_r.deadline),       
            "images":realt_images,
            "count_images":len(realt_images),  
            "add_date_time":formats.date_format(objects_r.add_date_time,"SHORT_DATETIME_FORMAT"), 
            "street":objects_r.street,
            "house":objects_r.house,
            "apartment":objects_r.apartment,  
            "price":str(objects_r.price),  
            "descriptions_short":objects_r.descriptions[:254],                   
            "descriptions_full":objects_r.descriptions,
            "showe_btn":True if (len(objects_r.descriptions) > 254) else False,
            "stay_in":str(objects_r.stay_in),
            "number_rooms":str(objects_r.number_rooms),
            "total_area":str(objects_r.total_area),  
            "size_room":str(objects_r.size_room),  
            "bargain":objects_r.bargain,
            "deposit":objects_r.deposit,
            "floor":objects_r.floor,
            "storey":objects_r.storey,
            "type_house":str(objects_r.type_house),
            "loggia_number":str(objects_r.loggia_number),
            "balcony_number":str(objects_r.balcony_number),
            "pepairs":str(objects_r.pepairs),
            "year_construction":objects_r.year_construction,
            "output_window":str(objects_r.output_window),
            "toilet":str(objects_r.toilet),
            "presence_furniture":str(objects_r.presence_furniture),
            "appliances":list(objects_r.appliances.all().values('title')),
            "living_area":objects_r.living_area,
            "kitchen_area":objects_r.kitchen_area,
            "land_area":objects_r.land_area,
            "electricity":str(objects_r.electricity),
            "heating":str(objects_r.heating),
            "water_supply":str(objects_r.water_supply),
            "security":objects_r.security,
            "banya":objects_r.banya,
            "pool":objects_r.pool,
            "outbuildings":objects_r.outbuildings,
            "category_land":str(objects_r.category_land),
            "cadastral_number":objects_r.cadastral_number,
            "distance_town":objects_r.distance_town,
            "sewage":str(objects_r.sewage),
            "garage_type":str(objects_r.garage_type),
            "type_parking_spaces":str(objects_r.type_parking_spaces),
            "denomination":objects_r.denomination,
            "room_condition":str(objects_r.room_condition),
            "category":str(objects_r.category),
            "type_structure":str(objects_r.type_structure),
            "class_structure":str(objects_r.class_structure),
            "building_condition":str(objects_r.building_condition),
            "ceiling_height":objects_r.ceiling_height,
            "elevator":objects_r.elevator,
            "parking":objects_r.parking,
            "internet":objects_r.internet,
            "microdistrict":str(objects_r.microdistrict)                                        
                             

        })   
        # формирования json шаблона  

    # формирования объявления 


    response_json = {             

        "items":realt_template,      
        "ThisPage":ThisPage,
        "CountPages":CountPages,      
        "LeftBarPage":LeftBarPage,    
        "RightBarPage":RightBarPage        
    }                                                                              
 

    return HttpResponse(json.dumps(response_json,ensure_ascii=False),content_type="application/json")  



def del_my_add(request,id_add):                  

    user_session_id = request.session.get('USER',False)
    
    if not user_session_id:    
        return redirect('/')           

    realtObject.objects.filter(user=user_session_id,pk=id_add).delete()                   


    return redirect('/realt/mylist/')   


