# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0029_typecommerce'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='type_commerce',
            field=models.ForeignKey(null=True, verbose_name='тип коммерции', to='realEstate.TypeCommerce', blank=True),
        ),
    ]
