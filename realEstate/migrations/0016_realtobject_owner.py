# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0015_auto_20160414_0208'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='owner',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
