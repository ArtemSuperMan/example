# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0003_auto_20160315_2143'),
    ]

    operations = [
        migrations.CreateModel(
            name='Toilet',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Санузел',
                'verbose_name': 'Санузел',
            },
        ),
        migrations.AddField(
            model_name='realtobject',
            name='toilet',
            field=models.ForeignKey(to='realEstate.Toilet', null=True, blank=True, verbose_name='Санузел'),
        ),
    ]
