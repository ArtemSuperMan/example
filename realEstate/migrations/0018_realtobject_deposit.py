# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0017_auto_20160418_0146'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='deposit',
            field=models.IntegerField(blank=True, null=True, default=1, verbose_name='цена'),
        ),
    ]
