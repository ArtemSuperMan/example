# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0014_auto_20160413_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtimages',
            name='image',
            field=imagekit.models.fields.ProcessedImageField(upload_to='images/', blank=True, verbose_name='Фотографии'),
        ),
    ]
