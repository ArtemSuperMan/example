# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0022_auto_20160423_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='deposit',
            field=models.IntegerField(blank=True, verbose_name='цена ', null=True, default=1),
        ),
    ]
