# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0013_auto_20160413_1343'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='fio',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='phone',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
