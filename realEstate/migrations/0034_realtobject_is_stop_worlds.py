# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0033_auto_20160528_1821'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='is_stop_worlds',
            field=models.BooleanField(default=False, verbose_name='Стоп слова'),
        ),
    ]
