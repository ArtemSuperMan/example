# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0030_realtobject_type_commerce'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='electricity',
            field=models.ForeignKey(blank=True, to='realEstate.Electricity', null=True, verbose_name='Электричество'),
        ),
    ]
