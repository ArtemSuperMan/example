# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('realEstate', '0012_auto_20160327_1849'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='realtobject',
            name='user_id',
        ),
        migrations.AddField(
            model_name='realtobject',
            name='user',
            field=models.ForeignKey(null=True, blank=True, to='users.Users', verbose_name='Пользователь'),
        ),
    ]
