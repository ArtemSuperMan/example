# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0023_auto_20160423_1640'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='realtobject',
            name='floor',
        ),
        migrations.RemoveField(
            model_name='realtobject',
            name='storey',
        ),
    ]
