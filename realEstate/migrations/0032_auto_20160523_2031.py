# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0031_auto_20160502_0952'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='add_date_time',
            field=models.DateTimeField(null=True, auto_now_add=True, verbose_name='Дата'),
        ),
        migrations.AlterField(
            model_name='realtobject',
            name='tarif_type',
            field=models.CharField(blank=True, verbose_name='тип тарифа', max_length=200, choices=[('Rental_of_property', 'Аренда жилой'), ('Sale_of_residential', 'Продажа жилой'), ('Rent_commercial', 'Аренда коммерческой'), ('Sales_of_commercial', 'Продажа коммерческой'), ('Sale_and_lease_of_land', 'Продажа и аренда земельного участка')]),
        ),
    ]
