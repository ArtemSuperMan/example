# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0009_auto_20160316_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='appliances',
            field=models.ManyToManyField(verbose_name='Бытовая техника', blank=True, to='realEstate.Appliances'),
        ),
    ]
