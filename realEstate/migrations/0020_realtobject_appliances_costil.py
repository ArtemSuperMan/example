# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0019_auto_20160422_2229'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='appliances_costil',
            field=models.TextField(blank=True, verbose_name='Костыль для бытовой техники'),
        ),
    ]
