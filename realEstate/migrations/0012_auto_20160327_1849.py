# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0011_auto_20160327_1748'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='realtobject',
            name='tarif',
        ),
        migrations.AddField(
            model_name='realtobject',
            name='tarif_type',
            field=models.CharField(blank=True, max_length=200, verbose_name='тип тарифа'),
        ),
    ]
