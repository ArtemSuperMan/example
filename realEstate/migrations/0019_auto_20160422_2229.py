# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0018_realtobject_deposit'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Категория  (вторичный рынок, новостройка)', 'verbose_name_plural': 'Категория (вторичный рынок, новостройка)'},
        ),
    ]
