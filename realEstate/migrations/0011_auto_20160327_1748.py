# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0010_auto_20160317_0023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='presence_furniture',
            field=models.ForeignKey(verbose_name='Наличие мебели', to='realEstate.PresenceFurniture', null=True, blank=True),
        ),
    ]
