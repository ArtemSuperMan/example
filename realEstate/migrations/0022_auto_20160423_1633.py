# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0021_auto_20160423_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='deposit',
            field=models.CharField(blank=True, max_length=10, verbose_name='Сумма залога', null=True),
        ),
    ]
