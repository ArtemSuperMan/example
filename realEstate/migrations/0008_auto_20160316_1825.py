# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0007_auto_20160316_1812'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='appliances',
            field=models.ManyToManyField(to='realEstate.Appliances', related_name='applianc', verbose_name='Бытовая техника'),
        ),
    ]
