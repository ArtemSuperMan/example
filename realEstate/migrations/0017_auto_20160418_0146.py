# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0016_realtobject_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='apartment',
            field=models.CharField(verbose_name='Квартира', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='house',
            field=models.CharField(verbose_name='Дом', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='street',
            field=models.CharField(verbose_name='Улица', max_length=100, blank=True),
        ),
    ]
