# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0024_auto_20160423_1644'),
    ]

    operations = [
        migrations.AddField(
            model_name='realtobject',
            name='floor',
            field=models.CharField(max_length=10, verbose_name='Этаж', blank=True, null=True, default=1),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='storey',
            field=models.CharField(max_length=10, verbose_name='Этажность', blank=True, null=True, default=1),
        ),
    ]
