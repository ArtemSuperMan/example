# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0025_auto_20160423_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='floor',
            field=models.IntegerField(verbose_name='Этаж', default=1, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='realtobject',
            name='storey',
            field=models.IntegerField(verbose_name='Этажность', default=1, blank=True, null=True),
        ),
    ]
