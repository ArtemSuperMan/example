# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0032_auto_20160523_2031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='kitchen_area',
            field=models.IntegerField(null=True, blank=True, default=0, verbose_name='Площадь Кухни'),
        ),
    ]
