# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0005_auto_20160315_2225'),
    ]

    operations = [
        migrations.CreateModel(
            name='Electricity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Электричество',
                'verbose_name_plural': 'Электричество',
            },
        ),
        migrations.AddField(
            model_name='realtobject',
            name='electricity',
            field=models.ForeignKey(verbose_name='Водоснабжение', to='realEstate.Electricity', null=True, blank=True),
        ),
    ]
