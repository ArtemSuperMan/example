# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Appliances',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Бытовая техника',
                'verbose_name': 'Бытовая техника ',
            },
        ),
        migrations.CreateModel(
            name='BalconyNumber',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Балкон (количество)',
                'verbose_name': 'Балкон (количество)',
            },
        ),
        migrations.CreateModel(
            name='BuildingCondition',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Состояние здания',
                'verbose_name': 'Состояние здания',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Категория',
                'verbose_name': 'Категория',
            },
        ),
        migrations.CreateModel(
            name='CategoryLand',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Категория земельного участка',
                'verbose_name': 'Категория земельного участка',
            },
        ),
        migrations.CreateModel(
            name='ClassStructure',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Класс строения',
                'verbose_name': 'Класс строения',
            },
        ),
        migrations.CreateModel(
            name='Deadline',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Период аренды',
                'verbose_name': 'Период аренды',
            },
        ),
        migrations.CreateModel(
            name='GarageType',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Тип гаража',
                'verbose_name': 'Тип гаража',
            },
        ),
        migrations.CreateModel(
            name='Heating',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Отопление',
                'verbose_name': 'Отопление',
            },
        ),
        migrations.CreateModel(
            name='LandCategor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Категория земель',
                'verbose_name': 'Категория земель',
            },
        ),
        migrations.CreateModel(
            name='LoggiaNumber',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Лоджия (количество)',
                'verbose_name': 'Лоджия (количество)',
            },
        ),
        migrations.CreateModel(
            name='Microdistrict',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Микро район',
                'verbose_name': 'Микро район',
            },
        ),
        migrations.CreateModel(
            name='NumberRooms',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Количество комнат',
                'verbose_name': 'Количество комнат',
            },
        ),
        migrations.CreateModel(
            name='OutputWindow',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Окна',
                'verbose_name': 'Окна',
            },
        ),
        migrations.CreateModel(
            name='Pepairs',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Ремонт',
                'verbose_name': 'Ремонт',
            },
        ),
        migrations.CreateModel(
            name='PresenceFurniture',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Наличие мебели',
                'verbose_name': 'Наличие мебели',
            },
        ),
        migrations.CreateModel(
            name='RealtImages',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('image', models.ImageField(blank=True, upload_to='images/', verbose_name='Фотографии')),
                ('index_image', models.CharField(blank=True, max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='realtObject',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('tarif', models.CharField(blank=True, max_length=200, verbose_name='Тариф')),
                ('is_public', models.BooleanField(default=False, verbose_name='Опубликовано')),
                ('user_id', models.IntegerField(blank=True, null=True, default=0, verbose_name='Аиди юзера')),
                ('type_object', models.CharField(blank=True, choices=[('Room', 'Комната'), ('Apartment', 'Квартира'), ('HouseDachaCottage', 'Дом,Дача,Коттедж'), ('Townhouse', 'Таунхаус'), ('Duplex', 'Дуплекс'), ('Cottage', 'Коттедж'), ('House', 'Дом'), ('Dacha', 'Дача'), ('Part_of_house', 'Часть дома'), ('Apart_of_house', 'Частичка дома'), ('Vremyanka', 'Времянка'), ('Production', 'Производство'), ('Stock', 'Склад'), ('FreeAssignment', 'Свободное назначение'), ('ReadyBusiness', 'Готовый бизнес'), ('GaragesParkingLots', 'Гаражи и машиноместа'), ('Land', 'Земельный участок'), ('Garages', 'Гараж'), ('Parking_spaces', 'Машиноместа')], max_length=100, verbose_name='Тип недвижимости')),
                ('type_transaction', models.CharField(blank=True, choices=[('Sale', 'Продажа'), ('Rent', 'Аренда')], max_length=100, verbose_name='Тип сделки')),
                ('toilet', models.CharField(blank=True, choices=[('InHome', 'В доме'), ('OnTheStreet', 'На улице'), ('Joint', 'Совместный'), ('Separated', 'Раздельный')], max_length=100, verbose_name='Санузел')),
                ('year_construction', models.CharField(blank=True, max_length=100, verbose_name='год постройки')),
                ('size_room', models.IntegerField(blank=True, null=True, default=0, verbose_name='Площадь комнаты')),
                ('total_area', models.IntegerField(blank=True, null=True, default=0, verbose_name='Площадь общая')),
                ('living_area', models.IntegerField(blank=True, null=True, default=0, verbose_name='Площадь жилая')),
                ('kitchen_area', models.IntegerField(blank=True, null=True, default=0, verbose_name='Площадь Кухня')),
                ('land_area', models.IntegerField(blank=True, null=True, default=0, verbose_name='площадь участка')),
                ('electricity', models.BooleanField(default=False, verbose_name='Электричество')),
                ('is_phone_obj', models.BooleanField(default=False, verbose_name='Телефон')),
                ('security', models.BooleanField(default=False, verbose_name='Охрана')),
                ('banya', models.BooleanField(default=False, verbose_name='Баня')),
                ('garage', models.BooleanField(default=False, verbose_name='Гараж')),
                ('pool', models.BooleanField(default=False, verbose_name='Бассейн')),
                ('outbuildings', models.BooleanField(default=False, verbose_name='Хозяйственные постройки')),
                ('floor', models.CharField(blank=True, max_length=10, verbose_name='Этаж')),
                ('storey', models.CharField(blank=True, max_length=10, verbose_name='Этажность')),
                ('denomination', models.CharField(blank=True, max_length=100, verbose_name='Наименование кафе, гостиница')),
                ('ceiling_height', models.IntegerField(blank=True, null=True, default=0, verbose_name='Высота поталков в м')),
                ('elevator', models.BooleanField(default=False, verbose_name='Лифт')),
                ('parking', models.BooleanField(default=False, verbose_name='Парковка')),
                ('internet', models.BooleanField(default=False, verbose_name='Интернет')),
                ('cadastral_number', models.CharField(blank=True, max_length=100, verbose_name='Кадастровый номер')),
                ('distance_town', models.IntegerField(blank=True, null=True, default=0, verbose_name='Расстояние до города км')),
                ('gas', models.BooleanField(default=False, verbose_name='Газ')),
                ('price', models.IntegerField(blank=True, null=True, default=1, verbose_name='цена')),
                ('bargain', models.BooleanField(default=False, verbose_name='торг')),
                ('descriptions', models.TextField(blank=True, verbose_name='Дополнительные сведения')),
                ('mai_image', models.ImageField(blank=True, upload_to='images/', verbose_name='Главное фото')),
                ('appliances', models.ManyToManyField(blank=True, to='realEstate.Appliances', verbose_name='Бытовая техника')),
                ('balcony_number', models.ForeignKey(blank=True, to='realEstate.BalconyNumber', verbose_name='Балкон (количество)', null=True)),
                ('building_condition', models.ForeignKey(blank=True, to='realEstate.BuildingCondition', verbose_name='Состояние  здания', null=True)),
                ('category', models.ForeignKey(blank=True, to='realEstate.Category', verbose_name='Категория', null=True)),
                ('category_land', models.ForeignKey(blank=True, to='realEstate.CategoryLand', verbose_name='Категория земельного участка', null=True)),
                ('class_structure', models.ForeignKey(blank=True, to='realEstate.ClassStructure', verbose_name='Класс строения', null=True)),
                ('deadline', models.ForeignKey(blank=True, to='realEstate.Deadline', verbose_name='Период аренды', null=True)),
                ('garage_type', models.ForeignKey(blank=True, to='realEstate.GarageType', verbose_name='Тип гаража', null=True)),
                ('heating', models.ForeignKey(blank=True, to='realEstate.Heating', verbose_name='Отопление', null=True)),
                ('land_category', models.ForeignKey(blank=True, to='realEstate.LandCategor', verbose_name='Категория земель', null=True)),
                ('loggia_number', models.ForeignKey(blank=True, to='realEstate.LoggiaNumber', verbose_name='Лоджия (количество)', null=True)),
                ('microdistrict', models.ForeignKey(blank=True, to='realEstate.Microdistrict', verbose_name='Микро район', null=True)),
                ('number_rooms', models.ForeignKey(blank=True, to='realEstate.NumberRooms', verbose_name='Количество комнат', null=True)),
                ('output_window', models.ForeignKey(blank=True, to='realEstate.OutputWindow', verbose_name='Окна', null=True)),
                ('pepairs', models.ForeignKey(blank=True, to='realEstate.Pepairs', verbose_name='Ремонт', null=True)),
                ('presence_furniture', models.ForeignKey(blank=True, to='realEstate.PresenceFurniture', verbose_name='Мебель', null=True)),
            ],
            options={
                'verbose_name_plural': 'Объекты недвижимости',
                'verbose_name': 'Объекты недвижимости',
            },
        ),
        migrations.CreateModel(
            name='ReasonsForBlocking',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Причины блокировки',
                'verbose_name': 'Причины блокировки',
            },
        ),
        migrations.CreateModel(
            name='RoomCondition',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Состояние  помещения',
                'verbose_name': 'Состояние  помещения',
            },
        ),
        migrations.CreateModel(
            name='Sewage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Канализация',
                'verbose_name': 'Канализация',
            },
        ),
        migrations.CreateModel(
            name='StayIn',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Комната в',
                'verbose_name': 'Комната в',
            },
        ),
        migrations.CreateModel(
            name='TypeHouse',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Тип дома',
                'verbose_name': 'Тип дома',
            },
        ),
        migrations.CreateModel(
            name='TypeLandArea',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Тип величены  ( земельного участка)',
                'verbose_name': 'Тип величены  ( земельного участка)',
            },
        ),
        migrations.CreateModel(
            name='TypeParkingSpaces',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Тип машиноместа',
                'verbose_name': 'Тип машиноместа',
            },
        ),
        migrations.CreateModel(
            name='TypeStructure',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Тип строения',
                'verbose_name': 'Тип строения',
            },
        ),
        migrations.CreateModel(
            name='WaterSupply',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Водоснабжение',
                'verbose_name': 'Водоснабжение',
            },
        ),
        migrations.AddField(
            model_name='realtobject',
            name='reasons_blocking',
            field=models.ForeignKey(blank=True, to='realEstate.ReasonsForBlocking', verbose_name='Причины блокировки', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='room_condition',
            field=models.ForeignKey(blank=True, to='realEstate.RoomCondition', verbose_name='Состояние  помещения', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='sewage',
            field=models.ForeignKey(blank=True, to='realEstate.Sewage', verbose_name='Канализация', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='stay_in',
            field=models.ForeignKey(blank=True, to='realEstate.StayIn', verbose_name='Комната в', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='type_house',
            field=models.ForeignKey(blank=True, to='realEstate.TypeHouse', verbose_name='Тип дома', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='type_land_area',
            field=models.ForeignKey(blank=True, to='realEstate.TypeLandArea', verbose_name='Тип величены(земельного участка)', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='type_parking_spaces',
            field=models.ForeignKey(blank=True, to='realEstate.TypeParkingSpaces', verbose_name='Тип машиноместа', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='type_structure',
            field=models.ForeignKey(blank=True, to='realEstate.TypeStructure', verbose_name='Тип строения', null=True),
        ),
        migrations.AddField(
            model_name='realtobject',
            name='water_supply',
            field=models.ForeignKey(blank=True, to='realEstate.WaterSupply', verbose_name='Водоснабжение', null=True),
        ),
        migrations.AddField(
            model_name='realtimages',
            name='realt_object',
            field=models.ForeignKey(related_name='images_r', to='realEstate.realtObject'),
        ),
    ]
