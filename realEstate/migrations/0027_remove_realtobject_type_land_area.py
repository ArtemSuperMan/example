# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0026_auto_20160423_1647'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='realtobject',
            name='type_land_area',
        ),
    ]
