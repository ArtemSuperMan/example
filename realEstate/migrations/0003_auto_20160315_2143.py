# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0002_auto_20160315_2142'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='realtobject',
            name='toilet',
        ),
        migrations.DeleteModel(
            name='Toilet',
        ),
    ]
