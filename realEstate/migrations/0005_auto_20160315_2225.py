# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0004_auto_20160315_2144'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='typelandarea',
            options={'verbose_name_plural': 'Единица измерения ( земельного участка)', 'verbose_name': 'Единица измерения ( земельного участка)'},
        ),
        migrations.RemoveField(
            model_name='realtobject',
            name='electricity',
        ),
        migrations.AlterField(
            model_name='realtobject',
            name='type_land_area',
            field=models.ForeignKey(blank=True, verbose_name='Единица измерения (земельного участка)', null=True, to='realEstate.TypeLandArea'),
        ),
    ]
