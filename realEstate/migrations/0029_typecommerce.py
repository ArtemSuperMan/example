# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0028_delete_typelandarea'),
    ]

    operations = [
        migrations.CreateModel(
            name='TypeCommerce',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Типы коммерции',
                'verbose_name': 'Тип коммерции',
            },
        ),
    ]
