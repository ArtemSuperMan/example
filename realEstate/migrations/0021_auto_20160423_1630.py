# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0020_realtobject_appliances_costil'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtobject',
            name='deposit',
            field=models.CharField(verbose_name='Сумма залога', blank=True, null=True, max_length=10, default=1),
        ),
    ]
