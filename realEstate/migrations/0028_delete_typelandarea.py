# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realEstate', '0027_remove_realtobject_type_land_area'),
    ]

    operations = [
        migrations.DeleteModel(
            name='TypeLandArea',
        ),
    ]
