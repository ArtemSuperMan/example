from django.contrib import admin
from .models import *
from django import forms
from django.shortcuts import render_to_response
from django.template import RequestContext    
from webApp.settings import BASE_DIR
from django.contrib.messages import constants as messages
from django.forms import ModelForm 
from django.http import QueryDict
from django.db.models import Q,F  
from datetime import datetime 

from daterange_filter.filter import DateRangeFilter

from django.db.models import Max,Min,Sum,Avg       

                   
@admin.register(Microdistrict,TypeCommerce,Deadline,TypeHouse,StayIn,NumberRooms,LoggiaNumber,BalconyNumber,
 OutputWindow,Pepairs,Heating,PresenceFurniture,GarageType,Category,TypeStructure,ClassStructure,
 BuildingCondition,RoomCondition,
 TypeParkingSpaces,ReasonsForBlocking,Toilet,Appliances,CategoryLand,Electricity,Sewage,WaterSupply)          
class AdminObject(admin.ModelAdmin):   
    pass


class RealtImagesInline(admin.StackedInline):    
    model = RealtImages


class BlockingForm(forms.Form):     
    reasons_blocking = forms.ModelChoiceField(queryset=ReasonsForBlocking.objects.all())



# class test(ModelForm):
#     model = realtObject
#     fields = ['appliance']
    
#     def save(self,request=request,*args,**kwargs):

#         # costil_str = ''  
#         # for appliance in self.appliances.all():    
#         #     costil_str += str(appliance.pk)
#         # self.appliances_costil = costil_str  

#         super(self).save(*args,**kwargs)                 

 
# Админка объектов              
class AdminRealtObject(admin.ModelAdmin):

    # def Окна(self,obj):  

    #   option = ''              

    #   for test in obj.appliances.all():
    #       option += "<option>{}</option>".format(test.title)
        
    #   return "<select>{}</select>".format(option)  
    # #  ????  
    # Окна.allow_tags = True


    #     
    def make_off_published(self,request,queryset):

            if 'do_action' in request.POST:
                form = BlockingForm(request.POST)
                if form.is_valid():
                    reasons_blocking = form.cleaned_data['reasons_blocking']
                    updated = queryset.update(reasons_blocking=reasons_blocking)  
                    # messages.success(request,
                    #                  '{} товаров перемещено в категорию {}'.format(
                    #                                     updated,reasons_blocking))  
                    # Ничего не возвращаем, это вернет нас на список товаров     
                    return
            else:
                form = BlockingForm()
                queryset.update(is_public=False)    
            return render_to_response(BASE_DIR+'/realEstate/realEstate_layout/set_category.html',  
                {'title': u'Укажите причину блокировки',
                 'objects': queryset,
                 'form': form},RequestContext(request))
                                         
    
    make_off_published.short_description = "Заблокировать"         

    #   
    def make_published(self,request,queryset):  
        queryset.update(reasons_blocking='',is_public=True)     

    make_published.short_description = "Опубликовать"

    actions = [make_published,make_off_published]    

    
    inlines = [RealtImagesInline]              

    # radio_fields = {'type_object': admin.HORIZONTAL}               

    list_display = (

        # 'add_date_time','tarif_type','appliances_costil','owner','reasons_blocking',
        # 'pk','loggia_number','balcony_number','is_public',
        # 'type_transaction','descriptions','type_object','total_area',     
        # 'garage_type','price','bargain','output_window',
        # 'security','banya','garage','user_id'

         'pk','user','user_phone','user_ective','is_public','is_stop_worlds',
         'type_object','type_transaction','microdistrict',
         'street','house','apartment',
         'number_rooms','total_area','floor','storey',
         'descriptions','price','living_area','kitchen_area','denomination',
         'distance_town','category_land'  

        )

 
    list_filter = (

        'is_stop_worlds',
        'user','type_object','type_transaction','microdistrict',
           'number_rooms','category_land',
            ('add_date_time',DateRangeFilter)

        ,)      




    change_list_template = BASE_DIR+'/realEstate/realEstate_layout/change_list.html'


    def changelist_view(self,request,extra_context=None,**kwargs):


        q_dict = QueryDict(request.META['QUERY_STRING']).dict()
        
        # удаляем сортировку тоесть переменную o  
        try:   
            q_dict.pop('o')   
        except:
            pass
            
        try:   
            q_dict.pop('p')   
        except:
            pass       
        
        date_from = q_dict.get('drf__add_date_time__gte',False)
        date_to = q_dict.get('drf__add_date_time__lte',False)          

        date_from_date  = date_from.replace('.','-') if date_from else False 
        date_to_date  = date_to.replace('.','-') if date_to else False  

        if date_from_date:
            date_from_result = datetime.strptime(date_from_date,"%d-%m-%Y").__str__()
            q_dict.pop('drf__add_date_time__gte')
            q_dict.update({ "add_date_time__gte":date_from_result})

        else:
            try:
                q_dict.pop('drf__add_date_time__gte')
            except:
                pass  
                      
        if  date_to_date:  
            date_to_result = datetime.strptime(date_to_date, "%d-%m-%Y").__str__()
            q_dict.pop('drf__add_date_time__lte')
            q_dict.update({"add_date_time__lte":date_to_result})  
        else:
            try: 
                q_dict.pop('drf__add_date_time__lte')  
            except:
                pass        

        try:  
            Avg_price = int(realtObject.objects.filter(**q_dict).aggregate(Avg('price'))['price__avg'])
            Avg_total_area = int(realtObject.objects.filter(**q_dict).aggregate(Avg('total_area'))['total_area__avg'])
            Avg_total_one =  Avg_price/Avg_total_area
        except:
            Avg_total_one = 0 


  
        extra_context = {                 

            'count_result':realtObject.objects.filter(**q_dict).count(),
            'sum_price':realtObject.objects.filter(**q_dict).aggregate(Sum('price'))['price__sum'],
            'Avg_price':realtObject.objects.filter(**q_dict).aggregate(Avg('price'))['price__avg'],

            'sum_total_area':realtObject.objects.filter(**q_dict).aggregate(Sum('total_area'))['total_area__sum'],
            'Avg_total_area':realtObject.objects.filter(**q_dict).aggregate(Avg('total_area'))['total_area__avg'],
            'Avg_total_one':Avg_total_one                
             

        }                

        

        return super(AdminRealtObject, self).changelist_view(request,extra_context=extra_context) 


    
    class Media:

        js = [ 

            '/static/grappelli/jquery/jquery-2.1.4.min.js',  

           '/static/grappelli/js/init_time_picker.js',

        ]    
 


admin.site.register(realtObject,AdminRealtObject)
# Админка объектов  



