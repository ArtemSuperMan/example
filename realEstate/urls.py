from django.conf.urls import url
from . import views


#
urlpatterns = [        

    url(r'^filter/$', views.filter),
    url(r'^getform/$', views.get_add_form_filter),
    url(r'^getres/$', views.ajax_filter),
    url(r'^addadvt/$', views.add_advt),
    url(r'^mylist/$', views.my_add_list_nome),
    url(r'^mylist/getjson/$', views.get_my_add_list_json),
    url(r'^mylist/del/id/(?P<id_add>[0-9]+)$', views.del_my_add),    
    url(r'^myaddeddit/id/(?P<id_add>[0-9]+)/$', views.my_add_eddit),                   
]  
