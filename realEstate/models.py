from django.db import models
from users.models import *     
from base64 import b64encode  
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from tariffs.models import Tariffs
from PIL import Image
from django.conf import settings   


class Watermark(object):

    def process(self,image):         

        logoim = Image.open(settings.BASE_DIR+'/static/images/watermark.png') #transparent watemark image 
        image.paste(logoim,(70,250),logoim)

        return image


# Срок сдачи (сли аренда добавляем срок сдачи (на длительный срок, посуточно, на несколько месяцев))       
class Deadline(models.Model):                                                                    
  
    title  =  models.CharField(max_length=100)


    class Meta:    
        verbose_name = "Период аренды"
        verbose_name_plural = "Период аренды"       
    #   
    def __str__(self):       
        return self.title  


# Микро район                                        
class Microdistrict(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Микро район"        
        verbose_name_plural = "Микро район"      
    #   
    def __str__(self):           
        return self.title    


# Тип постройки  (дома)        
class TypeHouse(models.Model):     
 
    title  =  models.CharField(max_length=100)


    class Meta:    
        verbose_name = "Тип дома"   
        verbose_name_plural = "Тип дома"   

    #   
    def __str__(self):       
        return self.title

# Комната в                  
class StayIn(models.Model):

    class Meta:    
        verbose_name = "Комната в"
        verbose_name_plural = "Комната в"

    title  =  models.CharField(max_length=100)
    #   
    def __str__(self):         
        return self.title 


# Количество комнат                             
class NumberRooms(models.Model):

    title  =  models.CharField(max_length=100)

    class Meta:    
        verbose_name = "Количество комнат"
        verbose_name_plural = "Количество комнат"
    #   
    def __str__(self):         
        return self.title 

                
# type_commerce ( тип коммерции)                                         
class TypeCommerce(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Тип коммерции"        
        verbose_name_plural = "Типы коммерции"      
    #   
    def __str__(self):           
        return self.title   


# Категория земельного участка                                         
class CategoryLand(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Категория земельного участка"        
        verbose_name_plural = "Категория земельного участка"      
    #   
    def __str__(self):           
        return self.title 


# Канализация                                     
class Sewage(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Канализация"  
        verbose_name_plural = "Канализация"      
    #   
    def __str__(self):           
        return self.title


# Водоснабжение                                         
class WaterSupply(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Водоснабжение"        
        verbose_name_plural = "Водоснабжение"      
    #   
    def __str__(self):           
        return self.title 


# Отопление (центральное газовое, угольное, печь, камин, без отопления)                                      
class Electricity(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:    
        verbose_name = "Электричество"  
        verbose_name_plural = "Электричество"
    #   
    def __str__(self):         
        return self.title



# Отопление (центральное газовое, угольное, печь, камин, без отопления)                                      
class Heating(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:    
        verbose_name = "Отопление"  
        verbose_name_plural = "Отопление"
    #   
    def __str__(self):         
        return self.title


#  Лоджия (количество)                                  
class LoggiaNumber(models.Model):

    title  =  models.CharField(max_length=100)

    class Meta:    
        verbose_name = "Лоджия (количество)"
        verbose_name_plural = "Лоджия (количество)"  

    #   
    def __str__(self):         
        return self.title


# Балкон (количество)                                        
class BalconyNumber(models.Model):

    title  =  models.CharField(max_length=100)

    class Meta:    
        verbose_name = "Балкон (количество)"
        verbose_name_plural = "Балкон (количество)"
    #   
    def __str__(self):         
        return self.title


# Окна (двор, улица, двор + улица)                                     
class OutputWindow(models.Model):         

    title = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Окна"  
        verbose_name_plural = "Окна"  

    #   
    def __str__(self):         
        return self.title 


# Ремонт (косметический, евро, дизайнерский, евро, отсутствует)                                     
class Pepairs(models.Model):         

    title  =  models.CharField(max_length=100)

    class Meta:    
        verbose_name = "Ремонт"  
        verbose_name_plural = "Ремонт"  
    #   
    def __str__(self):         
        return self.title 


# (Мебель Есть, нет, частично)                                        
class PresenceFurniture(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Наличие мебели"    
        verbose_name_plural = "Наличие мебели"  
    #   
    def __str__(self):           
        return self.title


# Бытовая техника                                      
class Appliances(models.Model):             

    title  =  models.CharField(max_length=100)

    class Meta:      
        verbose_name = "Бытовая техника "  
        verbose_name_plural = "Бытовая техника"

                
    #   
    def __str__(self):           
        return self.title 
        

#Состояние помещения(удовлетворительное, хорошее, отличное)    
class RoomCondition(models.Model):                                                                       
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Состояние  помещения"     
        verbose_name_plural = "Состояние  помещения"       
    #   
    def __str__(self):       
        return self.title 


#Состояние здания(удовлетворительное, хорошее, отличное)    
class BuildingCondition(models.Model):                                                                      
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Состояние здания"  
        verbose_name_plural = "Состояние здания"       
    #   
    def __str__(self):       
        return self.title         

#Категория (вторичный рынок, новостройка) 
class Category(models.Model):                                                                    
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Категория  (вторичный рынок, новостройка)"     
        verbose_name_plural = "Категория (вторичный рынок, новостройка)"           
    #   
    def __str__(self):       
        return self.title

#Тип строения (бизнес центр, торгово-развлекательный комплекс,нежилой фонд,жилой фонд,старый фонд)  
class TypeStructure(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Тип строения"     
        verbose_name_plural = "Тип строения"       
    #   
    def __str__(self):       
        return self.title

#Класс строения (А, А+, В, В+, С, С+)  
class ClassStructure(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Класс строения"     
        verbose_name_plural = "Класс строения"       
    #   
    def __str__(self):       
        return self.title

#Категория земель (ИЖС, Сельхозназначения , Промназначения, СНТ, ДНТ) 
class LandCategor(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Категория земель"     
        verbose_name_plural = "Категория земель"       
    #   
    def __str__(self):       
        return self.title

#Тип гаража 
class GarageType(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Тип гаража"     
        verbose_name_plural = "Тип гаража"       
    #   
    def __str__(self):       
        return self.title

#Тип машиноместа 
class TypeParkingSpaces(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Тип машиноместа"     
        verbose_name_plural = "Тип машиноместа"       
    #   
    def __str__(self):       
        return self.title


#Санузел 
class Toilet(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Санузел"     
        verbose_name_plural = "Санузел"       
    #   
    def __str__(self):       
        return self.title                


#Причины блокировки          
class ReasonsForBlocking(models.Model):                                                                         
  
    title  =  models.CharField(max_length=100)

    class Meta:       
        verbose_name = "Причины блокировки"     
        verbose_name_plural = "Причины блокировки"         
    #   
    def __str__(self):       
        return self.title        



# поля всех объектов                                                                                                                                                                      
class realtObject(models.Model):


    choices_values = {               

       'Room':'Комната',                                   
       'Smallfamily':'Малосимейка',                
       'Apartment':'Квартира',       
       'House':'Дом',
       'Dacha':'Дача', 
       'Part_of_house':'Часть дома',
       'Apart_of_house':'Частичка дома',
       'Vremyanka':'Времянка',
       'Townhouse':'Таунхаус',
       'Duplex':'Дуплекс',    
       'Cottage':'Коттедж',
       'Production':'Производство',
       'Stock':'Склад',  
       'FreeAssignment':'Свободное назначение',
       'ReadyBusiness':'Готовый бизнес',
       'Garages':'Гараж',  
       'Parking_spaces':'Машиноместа', 
       'Land':'Земельный участок', 

       'Sale':'Продажа',    
       'Rent':'Аренда'  

    }       


    # Тип недвижимости                      
    OJECTS_TYPE = (                                                

            ('Room','Комната',),               
            ('Apartment','Квартира',),                
            ('HouseDachaCottage','Дом,Дача,Коттедж',),
            ('Townhouse','Таунхаус',),
            ('Duplex','Дуплекс',),
            ('Cottage','Коттедж',),
            ('House','Дом',),
            ('Dacha','Дача',),
            ('Part_of_house','Часть дома',),
            ('Apart_of_house','Частичка дома',),
            ('Vremyanka','Времянка',),   
            ('Production','Производство',),
            ('Stock','Склад',), 
            ('FreeAssignment','Свободное назначение',),
            ('ReadyBusiness','Готовый бизнес',),
            ('GaragesParkingLots','Гаражи и машиноместа',),
            ('Land','Земельный участок',),
            ('Garages','Гараж',),
            ('Parking_spaces','Машиноместа',),                 
    
    )                

    # Тип сделки   
    TRANSACTION_TYPE = (                                                      

        ('Sale','Продажа',),
        ('Rent','Аренда',),      
 
    )                

     
    # Причины блокировки       
    reasons_blocking = models.ForeignKey(ReasonsForBlocking,verbose_name='Причины блокировки',null=True,blank=True)      
    # Причины блокировки                            

    # тип тарифа                                                      
    tarif_type = models.CharField(verbose_name='тип тарифа',max_length=200,blank=True,choices=Tariffs.TARRIFFS_TYPE)              
    # тип тарифа                   

    # Опубликовано                                   
    is_public = models.BooleanField(verbose_name='Опубликовано',default=False,blank=True)              
    # Опубликовано 

    # Опубликовано                                   
    is_public = models.BooleanField(verbose_name='Опубликовано',default=False,blank=True)              
    # Опубликовано 

    is_stop_worlds = models.BooleanField(verbose_name='Стоп слова',default=False,blank=True)            

    # Аиди юзера                         
    # user_id = models.IntegerField('Аиди юзера',default=0,blank=True,null=True)
    user = models.ForeignKey(Users,verbose_name='Пользователь',null=True,blank=True)            
    # Аиди юзера
 
    # фейковые узеры           
    # Фио                                                                                          
    fio = models.CharField(max_length=100,blank=True)
    # телефон                         
    phone = models.CharField(max_length=100,blank=True)
    # кто добавил объявления админ или юзер                    
    owner = models.CharField(max_length=100,blank=True)      
    # фейковые узеры                                                                                                      

    # Тип недвижимости                  
    type_object = models.CharField('Тип недвижимости',max_length=100,choices=OJECTS_TYPE,blank=True)    
    # Тип недвижимости 

    
    # Тип сделки   
    type_transaction = models.CharField(verbose_name='Тип сделки',max_length=100,choices=TRANSACTION_TYPE,blank=True)                 
    # Тип сделки 

    # Санузел           
    toilet = models.ForeignKey(Toilet,verbose_name='Санузел',null=True,blank=True)   
    # Санузел  
    
    # Период аренды (сли аренда добавляем срок сдачи (на длительный срок, посуточно, на несколько месяцев)) 
    deadline = models.ForeignKey(Deadline,verbose_name='Период аренды',null=True,blank=True)   
    # срок сдачи (сли аренда добавляем срок сдачи (на длительный срок, посуточно, на несколько месяцев))   


    # адреса   адреса   адреса  адреса 

    # Микрорайон города                
    microdistrict =  models.ForeignKey(Microdistrict,verbose_name='Микро район',null=True,blank=True)
    # Микрорайон города             
    
    # Улица
    street = models.CharField(verbose_name='Улица',max_length=100,blank=True) 

    # Дом
    house = models.CharField(verbose_name='Дом',max_length=100,blank=True)

    # Квартира  
    apartment =  models.CharField(verbose_name='Квартира',max_length=100,blank=True)

    # адреса   адреса   адреса  адреса  

    # Тип постройки Тип дома (дома)                
    type_house = models.ForeignKey(TypeHouse,verbose_name='Тип дома',null=True,blank=True)   
    # Тип дома

    # Год постройки     
    year_construction  = models.CharField(verbose_name='год постройки',max_length=100,blank=True)
    # Год постройки   

    # Комната в       
    stay_in  = models.ForeignKey(StayIn,verbose_name='Комната в',null=True,blank=True)
    # Комната в   
  

    # Количество комнат   
    number_rooms =  models.ForeignKey(NumberRooms,verbose_name='Количество комнат',null=True,blank=True)    
    # Количество комнат  
    
    # Площадь комнаты !!!!!! важно !!!!! ( для комнаты типерь это площадь квартиры  )      
    size_room = models.IntegerField(verbose_name='Площадь комнаты',default=0,blank=True,null=True)         

    # Площадь общая  ( Общая площадь квартиры  )  
    total_area = models.IntegerField(verbose_name='Площадь общая',default=0,blank=True,null=True)    
    # Площадь общая   

    # Площадь жилая       
    living_area = models.IntegerField(verbose_name='Площадь жилая',default=0,blank=True,null=True)
    # Площадь жилая   

    # Площадь  кухня            
    kitchen_area = models.IntegerField(verbose_name='Площадь Кухни',default=0,blank=True,null=True)
    # Площадь  кухня   

    # Размер земельного участка   для других объектов    
    land_area = models.IntegerField(verbose_name='площадь участка',default=0,blank=True,null=True)      
    # Размер земельного участка   

    # тип коммерции  
    type_commerce = models.ForeignKey(TypeCommerce,verbose_name='тип коммерции',null=True,blank=True)

    # Категория земельного участка                      
    category_land = models.ForeignKey(CategoryLand,verbose_name='Категория земельного участка',null=True,blank=True) 

    # Электричество   
    electricity  = models.ForeignKey(Electricity,verbose_name='Электричество',null=True,blank=True)       

    # Телефон  (yes/no)              
    is_phone_obj = models.BooleanField(verbose_name='Телефон',default=False,blank=True) 

    # Канализация 
    sewage  =  models.ForeignKey(Sewage,verbose_name='Канализация',null=True,blank=True) 

    # Водоснабжение                                                                         
    water_supply  =  models.ForeignKey(WaterSupply,verbose_name='Водоснабжение',null=True,blank=True)

    # Отопление (центральное газовое, угольное, печь, камин, без отопления)     
    heating = models.ForeignKey(Heating,verbose_name='Отопление',null=True,blank=True)  
    # Отопление (центральное газовое, угольное, печь, камин, без отопления)

    # Охрана                       
    security = models.BooleanField(verbose_name='Охрана',default=False,blank=True) 

    # Баня
    banya   = models.BooleanField(verbose_name='Баня',default=False,blank=True)

    # Гараж
    garage = models.BooleanField(verbose_name='Гараж',default=False,blank=True)

    # Бассейн                                                 
    pool = models.BooleanField(verbose_name='Бассейн',default=False,blank=True) 

    # Хозяйственные постройки            
    outbuildings = models.BooleanField(verbose_name='Хозяйственные постройки',default=False,blank=True)    
            

    # Этаж                     
    floor = models.IntegerField(verbose_name='Этаж',default=1,null=True,blank=True)      
    # Этаж  

    # Этажность        
    storey = models.IntegerField(verbose_name='Этажность',default=1,null=True,blank=True)  
    # Этажность                 
    

    #  Лоджия (количество)    
    loggia_number = models.ForeignKey(LoggiaNumber,verbose_name='Лоджия (количество)',null=True,blank=True)                
    #  Лоджия (количество) 

    # Балкон (количество)                              
    balcony_number  =  models.ForeignKey(BalconyNumber,verbose_name='Балкон (количество)',null=True,blank=True)   
    # Балкон (количество)  
    
    # Окна (двор, улица, двор + улица)                            
    output_window  = models.ForeignKey(OutputWindow,verbose_name='Окна',null=True,blank=True)        
    # Окна (двор, улица, двор + улица)

    # Ремонт (косметический, евро, дизайнерский, евро, отсутствует)  
    pepairs  =  models.ForeignKey(Pepairs,verbose_name='Ремонт',null=True,blank=True)        
    # Ремонт (косметический, евро, дизайнерский, евро, отсутствует)

    # Наличие мебели              
    presence_furniture  = models.ForeignKey(PresenceFurniture,null=True,verbose_name='Наличие мебели',blank=True)  
    # Наличие мебели     

    # Бытовая техника      
    appliances  =  models.ManyToManyField(Appliances,verbose_name='Бытовая техника',blank=True)  

    # Наименование (пользователь пишет сам, но в ячейке светлым аписать: пример: кафе, гостиница)
    denomination = models.CharField('Наименование кафе, гостиница',max_length=100,blank=True)    

    #Состояние помещения (удовлетворительное, хорошее, отличное)  
    room_condition = models.ForeignKey(RoomCondition,verbose_name='Состояние  помещения',null=True,blank=True)

    #Состояние здания (удовлетворительное, хорошее, отличное)  
    building_condition = models.ForeignKey(BuildingCondition,verbose_name='Состояние  здания',null=True,blank=True)

    #Категория (вторичный рынок, новостройка)  
    category = models.ForeignKey(Category,verbose_name='Категория',null=True,blank=True)

    #Тип строения (бизнес центр, торгово-развлекательный комплекс,нежилой фонд,жилой фонд,старый фонд)  
    type_structure = models.ForeignKey(TypeStructure,verbose_name='Тип строения',null=True,blank=True)

    #Класс строения (А, А+, В, В+, С, С+)    
    class_structure = models.ForeignKey(ClassStructure,verbose_name='Класс строения',null=True,blank=True)

    #Высота поталков в м 
    ceiling_height = models.IntegerField(verbose_name='Высота поталков в м',default=0,blank=True,null=True)

    #Лифт              
    elevator = models.BooleanField(verbose_name='Лифт',default=False,blank=True)

    #Парковка      
    parking = models.BooleanField(verbose_name='Парковка',default=False,blank=True)

    #Интернет        
    internet = models.BooleanField(verbose_name='Интернет',default=False,blank=True)      

    #Категория земель (ИЖС, Сельхозназначения , Промназначения, СНТ, ДНТ)  
    land_category = models.ForeignKey(LandCategor,verbose_name='Категория земель',null=True,blank=True)

    # Кадастровый номер   
    cadastral_number = models.CharField('Кадастровый номер',max_length=100,blank=True)

    #Расстояние до города км       
    distance_town = models.IntegerField(verbose_name='Расстояние до города км',default=0,blank=True,null=True)

    #Газ
    gas = models.BooleanField(verbose_name='Газ',default=False,blank=True)                    

    #Тип гаража        
    garage_type = models.ForeignKey(GarageType,verbose_name='Тип гаража',null=True,blank=True)

    #Тип машиноместа                 
    type_parking_spaces = models.ForeignKey(TypeParkingSpaces,verbose_name='Тип машиноместа',null=True,blank=True)

      
    # Цена                    
    price = models.IntegerField(verbose_name='цена',default=1,null=True,blank=True)     
    # Цена         

    # Возможен ли торг (или цена уже с учетом торга)      
    bargain = models.BooleanField(verbose_name='торг',default=False,blank=True)
    # Возможен ли торг (или цена уже с учетом торга)
    
    #Залог        
    deposit = models.IntegerField(verbose_name='цена ',default=1,null=True,blank=True)
    #Залог 

    # описания объекта (Дополнительные сведения в свободной форме )         
    descriptions = models.TextField(verbose_name='Дополнительные сведения',blank=True)      
    # Дополнительные сведения (текст от подающего объявление в свободной форме)

    # костыль для строгой фидьтрации по полю бытовая техника 
    appliances_costil = models.TextField(verbose_name='Костыль для бытовой техники',blank=True)


    # Главное фото                                                                                               
    mai_image = models.ImageField(upload_to='images/', blank=True, verbose_name='Главное фото')

    add_date_time = models.DateTimeField(verbose_name='Дата',auto_now_add=True,null=True)    

    #       
    def get_val_type_object(self):                            
        return self.choices_values.get(self.type_object)

    #       
    def get_val_type_transaction(self):      
         
        try:            
            transaction = self.choices_values[self.type_transaction]
        except:
            transaction = False  
        
        return transaction



    def user_phone(self):            

        try:
            return self.user.phone  
        except:           
            return self.phone      


    def user_ective(self):         
        try:
            return 'Активен'  if  self.user.is_active else 'Не активен!' 
        except:           
            return 'Аdmin'          

        

    def __str__(self):                   

        return self.type_object     


    class Meta:      
        verbose_name = "Объекты недвижимости"  
        verbose_name_plural = "Объекты недвижимости" 


    def save(self,*args,**kwargs):


        # costil_str = ''
        # for appliance in self.appliances.all():  
        #     costil_str += str(appliance.pk)
        # self.appliances_costil = costil_str                                      

        #               
        self.tarif_type = 'не совпало'              

        # типы жилой недвижимости    
        type_object_residential = ['Apartment','Room','House','Dacha','Part_of_house','Apart_of_house','Vremyanka','Townhouse','Duplex','Cottage']
        
        # типы коммерческой недвижимости    
        type_object_commercial = ['Production','Stock','FreeAssignment','ReadyBusiness','Garages','Parking_spaces']      


        if (self.type_object in type_object_residential) and self.type_transaction=='Sale':
            self.tarif_type = 'Sale_of_residential'
        elif  (self.type_object in type_object_residential) and self.type_transaction=='Rent':  
            self.tarif_type = 'Rental_of_property'  

        elif  (self.type_object in type_object_commercial) and self.type_transaction=='Rent':  
            self.tarif_type = 'Rent_commercial'
        elif  (self.type_object in type_object_commercial) and self.type_transaction=='Sale':  
            self.tarif_type = 'Sales_of_commercial'

        elif  (self.type_object == 'Land') and (self.type_transaction=='Sale' or self.type_transaction=='Rent'):  
            self.tarif_type = 'Sale_and_lease_of_land' 
        #  

        if self.user_id:                  
            self.owner = 'user'
            self.is_public  = False
        else:   
            self.owner = 'admin'
            self.is_public  = True

    
        super(realtObject,self).save(*args,**kwargs)
           


# Фотографии                                                                                                                  
class RealtImages(models.Model):                                                             

    realt_object = models.ForeignKey(realtObject,related_name='images_r')                   
    

    image = ProcessedImageField(upload_to='images/',blank=True, verbose_name='Фотографии',
                                           processors=[ResizeToFill(540,540),Watermark()],  
                                           format='JPEG',  
                                           options={'quality':90})

    index_image = models.CharField(max_length=100,blank=True)
    
    #  нада оптимизировать !!!!!!!!!!!!!!!!!!!!!!!!     
    def get_urlData(self):
        f = open(self.image.path, 'rb')   
        strf = b64encode(f.read())  
        f.close()             
        return strf              

    #   
    def __str__(self):              
        return self.image.url   