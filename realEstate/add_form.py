from django.forms import ModelForm  
from realEstate.models import realtObject
    

class RealtObjectForm(ModelForm):  


    class Meta:        
        model = realtObject
        fields = '__all__'

            