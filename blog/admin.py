from django.contrib import admin
from .models import *

#   
@admin.register(ChildrenCategory,Article)                    
class AdminBlog(admin.ModelAdmin):    

    class Media:      
        js = [ 
        	'/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js', '/static/grappelli/tinymce_setup/tinymce_setup.js',
        	'/static/grappelli/tinymce_setup/content_typography.css'  
        ]  
 

@admin.register(ParentCategory)                        
class AdminParentCategory(admin.ModelAdmin):                               

    list_display = ('title','descriptions','position',) # ????
    list_editable = ('position',)

    class Media:       
        js = ['/static/js/admin_list_reorder.js',]    
