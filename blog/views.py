from django.db.models import Q
from blog.models import *
from advertising.models import *
from django.http import HttpResponse
from django.template import RequestContext    
from django.shortcuts import render_to_response
from django.core import serializers
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger  
# from django.template.loader import render_to_string          

#                                                                  
def home(request):                

    blog = ParentCategory.objects.all().order_by('position')              
    
    return render_to_response('blog_home.html',{"blog":blog},RequestContext(request))


#                              
def list(request,blog_id):                                   
    
    blog = ParentCategory.objects.get(pk=blog_id)     

    blog_cat = ChildrenCategory.objects.filter(parent_category=blog_id)
    
    articles = Article.objects.filter(category=blog_cat)  


    advertising_blog_Left = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='Left')
    advertising_blog_bottom = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='bottom')  


    # пост навигация             
    paginator = Paginator(articles,10)


    page_n = request.GET.get('page',1)            

    try:   
        articles_item = paginator.page(page_n) 
    except:
        articles_item = paginator.page(1)


    template_params = {           

        'paginator':paginator,
        "articles":articles_item,  
        "blog":blog,
        "blog_cat":blog_cat,
        "advertising_blog_Left":advertising_blog_Left,
        "advertising_blog_bottom":advertising_blog_bottom   

    }        



    return render_to_response('blog_list.html',template_params,RequestContext(request)) 



#                              
def list_search(request,blog_id):                                 
    
    blog = ParentCategory.objects.get(pk=blog_id)     

    blog_cat = ChildrenCategory.objects.filter(parent_category=blog_id)

    query = request.GET.get('search','').strip(' ') 

    search = request.GET.get('search','')

    if query:
        query_search = ''        
        for strEl in query.split():
            if(len(strEl)>3):
                query_search += strEl[1:len(strEl)-1]+'|'

            if(len(strEl)==3):
                query_search += strEl+'|'  
            

        query_search = query_search.strip('|')        
        
        articles = Article.objects.filter(category=blog_cat,title__iregex=r'('+query_search+')')      
    else:
        articles = Article.objects.filter(category=blog_cat)

    advertising_blog_Left = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='Left')
    advertising_blog_bottom = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='bottom')    


    # пост навигация        
    paginator = Paginator(articles,10)


    page_n = request.GET.get('page',1)            

    try:   
        articles_item = paginator.page(page_n) 
    except:
        articles_item = paginator.page(1)


    template_params = {           

        'paginator':paginator,
        "articles":articles_item,
        "search":search,  
        "blog":blog,
        "blog_cat":blog_cat,
        "advertising_blog_Left":advertising_blog_Left,
        "advertising_blog_bottom":advertising_blog_bottom   

    }        



    return render_to_response('blog_search.html',template_params,RequestContext(request))  





#                       
def list_cat(request,blog_id,cat_id):                            


    blog = ParentCategory.objects.get(pk=blog_id)     

    blog_cat = ChildrenCategory.objects.filter(parent_category=blog_id) 

    articles = Article.objects.filter(category=cat_id)

    advertising_blog_Left = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='Left')
    advertising_blog_bottom = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='bottom') 

    # пост навигация        
    paginator = Paginator(articles,10)


    page_n = request.GET.get('page',1)            

    try:   
        articles_item = paginator.page(page_n) 
    except:
        articles_item = paginator.page(1)


    template_params = {              

        'paginator':paginator,
        "articles":articles_item,
        "blog":blog,
        "blog_cat":blog_cat,
        "advertising_blog_Left":advertising_blog_Left,
        "advertising_blog_bottom":advertising_blog_bottom    

    }     

    return render_to_response('blog_list.html',template_params,RequestContext(request))



#                       
def article(request,blog_id,cat_id,article_id): 

    blog = ParentCategory.objects.get(pk=blog_id)     

    blog_cat = ChildrenCategory.objects.filter(parent_category=blog_id)                             


    article = Article.objects.get(pk=article_id)

    advertising_blog_Left = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='Left')
    advertising_blog_bottom = AdvertisingBlog.objects.filter(parent_category=blog_id,is_active=True,position='bottom') 

    template_params = {   

        "blog":blog,
        "blog_cat":blog_cat,
        "article":article,           
        "advertising_blog_Left":advertising_blog_Left,
        "advertising_blog_bottom":advertising_blog_bottom    

    }        

    return render_to_response('blog_views_articl.html',template_params,RequestContext(request))              