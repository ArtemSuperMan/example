from django.conf.urls import url
from . import views


#
urlpatterns = [               

    url(r'^$', views.home),
    url(r'^list/(?P<blog_id>[0-9]+)/$', views.list),
    url(r'^search/(?P<blog_id>[0-9]+)/$', views.list_search),      
    url(r'^list/(?P<blog_id>[0-9]+)/cat/(?P<cat_id>[0-9]+)/$', views.list_cat),
    url(r'^list/(?P<blog_id>[0-9]+)/cat/(?P<cat_id>[0-9]+)/article/(?P<article_id>[0-9]+)/$', views.article),  

]  
