# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20160505_1639'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'verbose_name': 'Статьи/Документ', 'verbose_name_plural': 'Статьи/Документ'},
        ),
        migrations.AlterModelOptions(
            name='childrencategory',
            options={'verbose_name': 'Категория темы', 'verbose_name_plural': 'Категория темы'},
        ),
        migrations.AlterModelOptions(
            name='parentcategory',
            options={'verbose_name': 'Тема портала', 'verbose_name_plural': 'Тема портала'},
        ),
        migrations.RemoveField(
            model_name='article',
            name='brief_description',
        ),
    ]
