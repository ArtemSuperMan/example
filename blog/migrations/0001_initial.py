# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=500, verbose_name='Название')),
                ('brief_description', models.TextField(verbose_name='Краткое описание')),
                ('descriptions', models.TextField(verbose_name='Полное описание')),
                ('image', models.ImageField(upload_to='images/', verbose_name='Фотография', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ChildrenCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=500, verbose_name='Категория темы')),
            ],
        ),
        migrations.CreateModel(
            name='ParentCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('position', models.IntegerField(null=True, blank=True, verbose_name='Позиция')),
                ('title', models.CharField(max_length=500, verbose_name='Тема портала')),
                ('descriptions', models.TextField(verbose_name='Описания темы')),
                ('image', models.ImageField(upload_to='images/', verbose_name='Фотография', blank=True)),
            ],
            options={
                'ordering': ('position',),
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='childrencategory',
            name='parent_category',
            field=models.ForeignKey(to='blog.ParentCategory', verbose_name='Тема портала'),
        ),
        migrations.AddField(
            model_name='article',
            name='category',
            field=models.ForeignKey(null=True, to='blog.ChildrenCategory', verbose_name='Категория темы'),
        ),
    ]
