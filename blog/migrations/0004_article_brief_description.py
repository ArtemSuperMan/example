# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20160506_1701'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='brief_description',
            field=models.TextField(null=True, verbose_name='Краткое описание'),
        ),
    ]
