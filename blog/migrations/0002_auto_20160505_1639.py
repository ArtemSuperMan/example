# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='document',
            field=models.FileField(upload_to='documents/', verbose_name='Документ', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='descriptions',
            field=models.TextField(verbose_name='Полное описание', blank=True),
        ),
    ]
