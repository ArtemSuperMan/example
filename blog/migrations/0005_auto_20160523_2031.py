# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_article_brief_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='brief_description',
            field=models.TextField(verbose_name='Краткое описание'),
        ),
    ]
