from django.db import models


# базовый класс для добавления в модель изменения порядка    
class Orderable(models.Model):          

    position = models.IntegerField('Позиция',blank = True,null=True)  

    def save(self, *args, **kwargs):  
        if self.position is None:
            try:
                last = self.objects.order_by('-position')[0]
                self.position = last.position + 1
            except:
                self.position = 0
        return super(Orderable, self).save(*args, **kwargs)

    class Meta:  
        abstract = True  
        ordering = ('position',)    


# Тема портала                   
class ParentCategory(Orderable):          

    title = models.CharField(verbose_name='Тема портала',max_length=500)

    descriptions = models.TextField(verbose_name='Описания темы')  

    image = models.ImageField(upload_to='images/', blank=True, verbose_name='Фотография')

    # image_icon = models.ImageField(upload_to='images/', blank=True, verbose_name='Иконка')

    class Meta:      
        verbose_name = "Тема портала"        
        verbose_name_plural = "Тема портала" 

    def __str__(self):                              
        return  self.title           

#           
class ChildrenCategory(models.Model):                         

    # Тема портала                                                       
    parent_category = models.ForeignKey(ParentCategory,verbose_name='Тема портала')  
  
    title = models.CharField(verbose_name='Категория темы',max_length=500) 

    class Meta:      
        verbose_name = "Категория темы"        
        verbose_name_plural = "Категория темы"      

    def __str__(self):        
        return  self.title


# Статьи                                                                                           
class Article(models.Model):                                   

    # Категория темы                                          
    category =  models.ForeignKey(ChildrenCategory,verbose_name='Категория темы',null=True)                                                                      
    
    title = models.CharField(verbose_name='Название',max_length=500)

    brief_description = models.TextField(verbose_name='Краткое описание')   

    descriptions = models.TextField(verbose_name='Полное описание',blank=True)            

    image = models.ImageField(upload_to='images/', blank=True, verbose_name='Фотография')  

    document = models.FileField(upload_to='documents/', blank=True, verbose_name='Документ') 


    class Meta:      
        verbose_name = "Статьи/Документ"        
        verbose_name_plural = "Статьи/Документ"              

    def __str__(self):  
        return  self.title    
